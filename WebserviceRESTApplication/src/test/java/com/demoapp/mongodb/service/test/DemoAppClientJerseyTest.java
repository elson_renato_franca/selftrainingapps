package com.demoapp.mongodb.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.restful.model.vo.ContactVO;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * 
 * 
 *
 */
public class DemoAppClientJerseyTest {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(DemoAppClientJerseyTest.class);

	/**
	 * 
	 */
	private String FIND_BY_NAME_URL = "http://localhost:8080/WebserviceRESTApplication/rest/contacthandler/findByName";

	/**
	 * 
	 */
	private String FIND_BY_ID_URL = "http://localhost:8080/WebserviceRESTApplication/rest/contacthandler/findById";

	/**
	 * 
	 */
	private String FIND_ALL_URL = "http://localhost:8080/WebserviceRESTApplication/rest/contacthandler/findAll";

	/**
	 * 
	 */
	private String SAVE_CONTACT = "http://localhost:8080/WebserviceRESTApplication/rest/contacthandler/saveContact";

	/**
	 * 
	 */
	private ApplicationContext context;

	/**
	 * 
	 */
	private Client client;

	/**
	 * 
	 */
	private ContactVO contactVO;

	@Before
	public void setup() {
		this.context = new ClassPathXmlApplicationContext("SpringBeans.xml");
		this.contactVO = (ContactVO) context.getBean("contactVO");
		this.client = Client.create();
	}

	@Test
	public void findByName() throws Exception {

		try {
			WebResource webResource = client.resource(FIND_BY_NAME_URL);
			String response = webResource.get(String.class);
			log.debug("Webservice Response - Find By Name: " + response);
			assertNotNull(response);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void findByID() throws Exception {

		try {
			WebResource webResource = client.resource(FIND_BY_ID_URL);
			String response = webResource.get(String.class);
			log.debug("Webservice Response - Find By ID: " + response);
			assertNotNull(response);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void saveContact() throws Exception {

		try { // Create a new contact StringBuffer newContact = new
			StringBuffer newContact = new StringBuffer();
			newContact.append("<contact>").append("<id>14</id>").append("<firsName>Alfred</firstName>")
					.append("<lastName>Nobel</lastName>").append("<age>189</age>")
					.append("<email>alfred.nobel@gmail.com</email>").append("<telephone>222222222222</telephone>")
					.append("</contact>");
			
			WebResource webResource = this.client.resource(SAVE_CONTACT);
			webResource.type(MediaType.APPLICATION_XML).accept(MediaType.APPLICATION_XML).post(ContactVO.class, this.contactVO);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @return
	 */
	public ApplicationContext getContext() {
		return context;
	}

	/**
	 * 
	 * @param context
	 */
	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	/**
	 * 
	 * @return
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * 
	 * @param client
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 * 
	 * @return
	 */
	public ContactVO getContactVO() {
		return contactVO;
	}

	/**
	 * 
	 * @param contactVO
	 */
	public void setContactVO(ContactVO contactVO) {
		this.contactVO = contactVO;
	}

	/**
	 * 
	 * @return
	 */
	public static Log getLog() {
		return log;
	}

}
