package com.demoapp.mongodb.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.backend.mongodb.service.ContactHandlerService;
import com.demoapp.restful.model.to.ContactTO;
import com.demoapp.restful.model.vo.ContactVO;


/**
 * 
 * 
 *
 */
public class ContactHandlerServiceTest {

	
	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(ContactHandlerServiceTest.class);
	
	/**
	 * 
	 */
	private ContactHandlerService contactService;
	
	/**
	 * 
	 */
	private ApplicationContext context;
	
	/**
	 * 
	 */
	private ContactVO contactVO;
	
	/*
	@Before
	public void setup(){
		
		this.context = new ClassPathXmlApplicationContext("SpringBeans.xml");
		this.contactService = (ContactHandlerService) context.getBean("contactHandlerService");
		this.contactVO =  (ContactVO) context.getBean("contactVO");
		
	}
	
	
	@Test
	public void saveContact(){
		
		try {
			int affectedRows = this.contactService.saveContact(this.contactVO);
			ContactTO persistedContact = this.contactService.findContactById(contactVO);
			log.debug("Contacted saved successfully: "+persistedContact);
			assertEquals(affectedRows,1);
			assertNotNull(persistedContact);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void findAll(){
		try {
			List<ContactTO> resultList = this.contactService.findAll();
			assertNotNull(resultList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	@Test
	public void findContactByName(){
		try {
			ContactTO resultQuery = this.contactService.findContactByName(this.contactVO);
			log.debug("Find by name: "+resultQuery.toString());
			assertNotNull(resultQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	@Test
	public void findContactById(){
		try {
			ContactTO resultQuery = this.contactService.findContactById(this.contactVO);
			log.debug("Find by ID: "+resultQuery.toString());
			assertNotNull(resultQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void updateContact(){
		try {
			int affectedRows = this.contactService.updateContact(this.contactVO);
			ContactTO resultQuery = this.contactService.findContactById(this.contactVO);
			assertEquals(affectedRows,1);
			assertNotNull(resultQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Test
	public void deleteContact(){
		
		try {
			int affectedRows = this.contactService.deleteContact(this.contactVO);
			ContactTO resultQuery = this.contactService.findContactById(this.contactVO);
			assertEquals(affectedRows,1);
			assertNull(resultQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	/**
	 * 
	 * @return
	 */
	public ContactHandlerService getContactService() {
		return contactService;
	}

	/**
	 * 
	 * @param contactService
	 */
	public void setContactService(ContactHandlerService contactService) {
		this.contactService = contactService;
	}

	/**
	 * 
	 * @return
	 */
	public ApplicationContext getContext() {
		return context;
	}

	/**
	 * 
	 * @param context
	 */
	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	/**
	 * 
	 * @return
	 */
	public ContactVO getContactVO() {
		return contactVO;
	}

	/**
	 * 
	 * @param contactVO
	 */
	public void setContactVO(ContactVO contactVO) {
		this.contactVO = contactVO;
	}


	
	
	
	
}
