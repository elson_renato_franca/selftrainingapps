package com.demoapp.backend.mongodb.dao.util;

/**
 * 
 * This class is responsible for holding all database queries
 *
 */
public interface DemoAppQueries {


	/**
	 * It references to persistent Unit provided by JPA 2.1. 
	 * For more details please refer to src/main/resource/META-INF/persistence.xml 
	 */
	public static final String PERSISTENCE_UNITY_NAME = "myPersistentUnit";
	
	/**
	 * JPQL query name - Searches for a specific user
	 */
	public static final String FIND_BY_NAME_QUERY = "from Contact c where c.firstName like :firstName";
	
}
