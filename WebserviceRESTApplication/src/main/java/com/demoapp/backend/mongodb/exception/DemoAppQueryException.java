package com.demoapp.backend.mongodb.exception;

/**
 * 
 * This classes maps the most common errors that can be found in an error 
 *
 */
public interface DemoAppQueryException {

	/**
	 * Thrown by the persistence provider when getSingleResult() is executed on a query and there is no result to return. 
	 * This exception will not cause the current transaction, if one is active, to be marked for roll back
	 */
	public static final String NO_RESULT_EXCEPTION="No result that matches the query was found in database: ";
	
	/**
	 * Thrown by the persistence provider when an entity reference obtained by EntityManager.getReference is accessed 
	 * but the entity does not exist. Thrown when EntityManager.refresh is called and the object no longer exists in the 
	 * database. Thrown when EntityManager.lock is used with pessimistic locking is used and the entity no longer exists 
	 * in the database. 
	 * 
	 */
	public static final String ENTITY_NOT_FOUND_EXCEPTION="This exception was caused by a non existent entity or by an entity that was deleted but not was not removed from a Collection that it is a member of.";
	
	/**
	 * This exception signals that a method has been invoked at an illegal or inappropriate time.
	 */
	public static final String ILLEGAL_STATE_EXCEPTION="This will be called for a Java Persistence query language UPDATE or DELETE statement";
	
	/**
	 * Thrown by the persistence provider when a query times out and only the statement is rolled back. 
	 * The current transaction, if one is active, will be not be marked for rollback. 
	 */
	public static final String QUERY_TIMEOUT_EXCEPTION="The query execution exceeds the query timeout value set and only the statement is rolled back";
	
	/**
	 * 
	 * If you use an EntityManager with a transaction persistence context model outside of an active transaction, 
	 * each method invocation creates a new persistence context, performs the method action, and ends the persistence context. 
	 * For example, consider using the EntityManager.find method outside of a transaction. The EntityManager will create a temporary persistence context, 
	 * perform the find operation, end the persistence context, and return the detached result object to you. 
	 * A second call with the same id will return a second detached object. 
	 */
	public static final String ILLEGAL_ARGUMENT_EXCEPTION="Removing a detached instance.No transaction is currently active. Your code is running outside a transaction context";

	/**
	 * Each SQLException provides several kinds of information:
	 * a string describing the error. This is used as the Java Exception message, available via the method getMesasge.
     * a "SQLstate" string, which follows either the XOPEN SQLstate conventions or the SQL:2003 conventions. 
     * The values of the SQLState string are described in the appropriate spec. The DatabaseMetaData method getSQLStateType can 
     * be used to discover whether the driver returns the XOPEN type or the SQL:2003 type.
     * an integer error code that is specific to each vendor. Normally this will be the actual error code returned by the 
     * underlying database.a chain to a next Exception. This can be used to provide additional error information.
     * the causal relationship, if any for this SQLException. 
	 */
	public static final String SQL_EXCEPTION="This exception is related to database access error or other errors. ";
	
	/**
	 * Implementation of JDBCException indicating that the requested DML operation resulted 
	 * in a violation of a defined integrity constraint.
	 */
	public static final String CONSTRAINT_VIOLATION_EXCEPTION="The operation violated an integrity constraint";
}
