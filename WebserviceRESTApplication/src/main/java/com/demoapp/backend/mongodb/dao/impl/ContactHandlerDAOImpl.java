package com.demoapp.backend.mongodb.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.demoapp.backend.mongodb.dao.ContactHandlerDAO;
import com.demoapp.backend.mongodb.dao.util.DemoAppQueries;
import com.demoapp.backend.mongodb.entities.Contact;

/**
 * 
 * Implements contact details service
 *
 */
public class ContactHandlerDAOImpl implements ContactHandlerDAO {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(ContactHandlerDAOImpl.class);

	/**
	 * Search for all contacts in MongoDB
	 * 
	 * @return List<Contact>
	 * @throws Exception
	 */
	public List<Contact> findAll() throws Exception {

		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(DemoAppQueries.PERSISTENCE_UNITY_NAME);
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		Query query = entityManager.createQuery("from Contact");
		List<Contact> contacts = query.getResultList();
		entityManager.close();
		return contacts;
	}

	/**
	 * Search for contacts in MongoDB according to a specific name
	 * 
	 * @return Contact
	 * @throws Exception
	 */
	public void updateContact(Contact contact) throws Exception {

		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(DemoAppQueries.PERSISTENCE_UNITY_NAME);
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		entityManager.merge(contact);
		transaction.commit();
		entityManager.close();

	}

	/**
	 * Remove contact details
	 * 
	 * @param Contact
	 *            contact
	 * @throws Exception
	 */
	public void deleteContact(Contact contact) throws Exception {

		log.debug("Removing the object by ID: " + contact.getId());

		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(DemoAppQueries.PERSISTENCE_UNITY_NAME);
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		Object managedObject = entityManager.merge(contact);
		entityManager.remove(managedObject);
		transaction.commit();
		entityManager.close();
		log.debug("Object with ID " + contact.getId() + " was removed successfully.");

	}

	/**
	 * Save contact
	 * 
	 * @param contact
	 * @throws Exception
	 */
	public void saveContact(Contact contact) throws Exception {

		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory(DemoAppQueries.PERSISTENCE_UNITY_NAME);
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction transaction = entityManager.getTransaction();
		log.debug("Persisting Contact object");
		transaction.begin();
		entityManager.persist(contact);
		transaction.commit();
		entityManager.close();

	}

	/**
	 * This method searches by Id
	 * 
	 * @param Contact
	 * @return Contact
	 * @throws Exception
	 */
	public Contact findContactById(Contact contact) throws Exception {

		Contact resultQuery = null;
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory(DemoAppQueries.PERSISTENCE_UNITY_NAME);
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		resultQuery = (Contact) entityManager.getReference(Contact.class, contact.getId().intValue());

		return resultQuery;

	}

	/**
	 * This method searches by name
	 * 
	 * @param contact
	 * @return
	 * @throws Exception
	 */
	public Contact findContactByName(Contact contact) throws Exception {

		Contact resultQuery = null;
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory(DemoAppQueries.PERSISTENCE_UNITY_NAME);
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		Query query = entityManager.createQuery(DemoAppQueries.FIND_BY_NAME_QUERY);
		query.setParameter("firstName", contact.getFirstName());
		resultQuery = (Contact) query.getSingleResult();
		log.debug(resultQuery.toString());

		return resultQuery;
	}

}
