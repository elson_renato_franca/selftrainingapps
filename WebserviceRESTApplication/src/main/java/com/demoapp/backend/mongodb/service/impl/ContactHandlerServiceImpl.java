package com.demoapp.backend.mongodb.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.QueryTimeoutException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.backend.mongodb.converter.ContactHandlerConverter;
import com.demoapp.backend.mongodb.dao.ContactHandlerDAO;
import com.demoapp.backend.mongodb.entities.Contact;
import com.demoapp.backend.mongodb.exception.DemoAppQueryException;
import com.demoapp.backend.mongodb.service.ContactHandlerService;
import com.demoapp.restful.model.to.ContactTO;
import com.demoapp.restful.model.vo.ContactVO;

/**
 * 
 * Implements contact details service
 *
 */
public class ContactHandlerServiceImpl implements ContactHandlerService {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(ContactHandlerServiceImpl.class);
	
	/**
	 * 
	 */
	private ContactHandlerConverter contactHandlerConverter;

	/**
	 * 
	 */
	private ContactHandlerDAO contactHandlerDAO;

	/**
	 * This method searches for all contacts in MongoDB
	 * 
	 * @return List<ContactTO>
	 * @throws Exception
	 */
	public List<ContactTO> findAll() throws Exception {
		List<ContactTO> contactTOList = null;
		
		try{
			List<Contact> contactListFromDb = this.contactHandlerDAO.findAll();
			
			if (contactListFromDb != null && !contactListFromDb.isEmpty()) {
				
				contactTOList = new ArrayList<ContactTO>();
				
				for (Contact contactEntity : contactListFromDb) {
					ContactTO contactTO = contactHandlerConverter.toContactJson(contactEntity);
					contactTOList.add(contactTO);
				}
			}
	
		} catch (QueryTimeoutException queryTimeoutException) {
			contactTOList = null;
			log.debug(DemoAppQueryException.QUERY_TIMEOUT_EXCEPTION + queryTimeoutException);
		} catch (IllegalStateException illegalStateException) {
			contactTOList = null;
			log.debug(DemoAppQueryException.ILLEGAL_STATE_EXCEPTION + illegalStateException);
		}

		
		return contactTOList;
	}

	/**
	 * Search for contacts in MongoDB according to a specific name
	 * 
	 * @return Contact
	 * @throws Exception
	 */
	public ContactTO findContactByName(ContactVO contactVO) throws Exception {

		ContactTO contactTO = null;

		try {
			Contact entityContact = contactHandlerConverter.toContactEntity(contactVO);
			Contact resultQuery = this.contactHandlerDAO.findContactByName(entityContact);
			contactTO = contactHandlerConverter.toContactJson(resultQuery);
		} catch (NoResultException nre) {
			contactTO = null;
			log.debug(DemoAppQueryException.NO_RESULT_EXCEPTION + nre);
		} catch (EntityNotFoundException entityNotFoundException) {
			contactTO = null;
			log.debug(DemoAppQueryException.ENTITY_NOT_FOUND_EXCEPTION + entityNotFoundException);
		}

		return contactTO;
	}

	/**
	 * This method searches for contacts in MongoDB according to a specific id
	 * 
	 * @param ContactVO
	 * @return ContactTO
	 * @throws Exception
	 */
	public ContactTO findContactById(ContactVO contactVO) throws Exception {

		ContactTO contactTO = null;
		try {
			Contact entityContact = this.contactHandlerConverter.toContactEntity(contactVO);
			Contact resultQuery = this.contactHandlerDAO.findContactById(entityContact);
			contactTO = this.contactHandlerConverter.toContactJson(resultQuery);
		} catch (NoResultException nre) {
			contactTO = null;
			log.debug(DemoAppQueryException.NO_RESULT_EXCEPTION + nre);
		} catch (EntityNotFoundException entityNotFoundException) {
			contactTO = null;
			log.debug(DemoAppQueryException.ENTITY_NOT_FOUND_EXCEPTION + entityNotFoundException);
		}

		return contactTO;
	}


	/**
	 * This method updates contact details
	 * @param contactVO contactVO
	 * @return affectedRows
	 * @throws Exception
	 */
	public int updateContact(ContactVO contactVO) throws Exception {
		
		int affectedRows = 0;
		
		try{
			ContactTO existingContact = this.findContactById(contactVO);
			
			if (existingContact != null) {
				Contact contact = this.contactHandlerConverter.toContactEntity(contactVO);
				this.contactHandlerDAO.updateContact(contact);
				affectedRows++;
			}	
		}catch (IllegalArgumentException e) {
			log.debug(DemoAppQueryException.ILLEGAL_ARGUMENT_EXCEPTION + e);
		}catch(SQLException ex){
			log.debug(DemoAppQueryException.SQL_EXCEPTION + ex);
		}
		
		return affectedRows;
		
	}


	/**
	 * This method removes the provided contactVO
	 * @param ContactVO contactVO
	 * @return affectedRows
	 * @throws Exception
	 */
	public int deleteContact(ContactVO contactVO) throws Exception {
		
		int affectedRows = 0;
		
		try{
			ContactTO existingContact = this.findContactById(contactVO);
		
			if (existingContact != null) {
				Contact contact = this.contactHandlerConverter.toContactEntity(contactVO);
				this.contactHandlerDAO.deleteContact(contact);
				affectedRows++;
			}
	
		}catch (IllegalArgumentException e) {
			log.debug(DemoAppQueryException.ILLEGAL_ARGUMENT_EXCEPTION + e);
		}catch(SQLException ex){
			log.debug(DemoAppQueryException.SQL_EXCEPTION + ex);
		}
		 
		return affectedRows;
	}

	/**
	 * This method saves the provided contactVO
	 * 
	 * @param contactVO
	 * @throws Exception
	 */
	public int saveContact(ContactVO contactVO) throws Exception {

		int affectedRows = 0;
		
		try{
			ContactTO existingContact = this.findContactById(contactVO);

			if (existingContact == null) {
				Contact contact = this.contactHandlerConverter.toContactEntity(contactVO);
				this.contactHandlerDAO.saveContact(contact);
				affectedRows++;
			}

		}catch(SQLException ex){
			log.debug(DemoAppQueryException.SQL_EXCEPTION + ex);
		}catch(ConstraintViolationException constraintViolationException){
			log.debug(DemoAppQueryException.CONSTRAINT_VIOLATION_EXCEPTION + constraintViolationException);
		}
		
		return affectedRows;
	}

	/**
	 * 
	 * @return
	 */
	public ContactHandlerDAO getContactHandlerDAO() {
		return contactHandlerDAO;
	}

	/**
	 * 
	 * @param contactHandlerDAO
	 */
	public void setContactHandlerDAO(ContactHandlerDAO contactHandlerDAO) {
		this.contactHandlerDAO = contactHandlerDAO;
	}

	/**
	 * 
	 * @return
	 */
	public ContactHandlerConverter getContactHandlerConverter() {
		ApplicationContext context = new ClassPathXmlApplicationContext("SpringBeans.xml");
		this.contactHandlerConverter =  (ContactHandlerConverter) context.getBean("contactHandlerConverter");
		return contactHandlerConverter;
	}

	/**
	 * 
	 * @param contactHandlerConverter
	 */
	public void setContactHandlerConverter(ContactHandlerConverter contactHandlerConverter) {
		this.contactHandlerConverter = contactHandlerConverter;
	}
	
	

}
