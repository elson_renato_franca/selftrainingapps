package com.demoapp.backend.mongodb.converter;

import com.demoapp.backend.mongodb.entities.Contact;
import com.demoapp.restful.model.to.ContactTO;
import com.demoapp.restful.model.vo.ContactVO;

/**
 * 
 * 
 *
 */
public class ContactHandlerConverter {

	/**
	 * 
	 * @param contact
	 * @return ContactTO
	 * @throws Exception
	 */
	public ContactTO  toContactJson(Contact contact)throws Exception{
		
		ContactTO contactTO = new ContactTO();
		
		contactTO.setAge(contact.getAge());
		contactTO.setEmail(contact.getEmail());
		contactTO.setFirstName(contact.getFirstName());
		contactTO.setId(contact.getId());
		contactTO.setLastName(contact.getLastName());
		contactTO.setTelephone(contact.getTelephone());
		
		return contactTO;
	}
	
	/**
	 * 
	 * @param contactVO
	 * @return
	 * @throws Exception
	 */
	public Contact toContactEntity(ContactVO contactVO)throws Exception{
		Contact contact = new Contact();
		
		contact.setAge(contactVO.getAge());
		contact.setEmail(contactVO.getEmail());
		contact.setFirstName(contactVO.getFirstName());
		contact.setId(contactVO.getId());
		contact.setLastName(contactVO.getLastName());
		contact.setTelephone(contactVO.getTelephone());
		
		return contact;
	}
	
}
