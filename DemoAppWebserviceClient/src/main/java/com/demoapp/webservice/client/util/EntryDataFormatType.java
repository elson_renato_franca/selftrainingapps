package com.demoapp.webservice.client.util;

/**
 * 
 * This class categorizes the entry data format type in two different categories:
 * 
 * - XML
 * - JSON
 *
 */
public interface EntryDataFormatType {

	/**
	 * 
	 */
	public static final String JSON_FORMAT="application/json";
	
	/**
	 * 
	 */
	public static final String XML_FORMAT="application/xml";
}
