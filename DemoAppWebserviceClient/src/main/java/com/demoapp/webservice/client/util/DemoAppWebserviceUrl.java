package com.demoapp.webservice.client.util;

/**
 * 
 * 
 *
 */
public interface DemoAppWebserviceUrl {

	/**
	 * 
	 */
	public static final String FIND_BY_NAME_URL = "http://localhost:8080/DemoAppRestWebservice/rest/contacthandler/findByName";

	/**
	 * 
	 */
	public static final String FIND_BY_ID_URL = "http://localhost:8080/DemoAppRestWebservice/rest/contacthandler/findById";

	/**
	 * 
	 */
	public static final String FIND_ALL_URL = "http://localhost:8080/DemoAppRestWebservice/rest/contacthandler/findAll";

	/**
	 * 
	 */
	public static final String SAVE_CONTACT_URL = "http://localhost:8080/DemoAppRestWebservice/rest/contacthandler/saveContact";
	
	/**
	 * 
	 */
	public static final String UPDATE_CONTACT_URL = "http://localhost:8080/DemoAppRestWebservice/rest/contacthandler/updateContact";
	

	/**
	 * 
	 */
	public static final String JSON_FILE_URL="C:\\Users\\efranca\\Development\\JerseyClientInput.json";
	
	/**
	 * 
	 */
	public static final String XML_FILE_URL="C:\\Users\\efranca\\Development\\JerseyClientInputXML.xml";
	
}
