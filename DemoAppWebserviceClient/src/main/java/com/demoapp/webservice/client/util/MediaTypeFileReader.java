package com.demoapp.webservice.client.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * This class converts JSON file to String object
 *
 */
public class MediaTypeFileReader {


	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(MediaTypeFileReader.class);
	
	/**
	 * This method reads each line of JSON file and converts to 
	 * String object.
	 * @param pathname
	 * @return String object
	 * @throws IOException
	 */
	public static String readFile(String pathname) throws Exception {
		
		File file = null;
		Scanner scanner = null;
	    String fileContentAsString = null;
	    
		try {
	    	file = new File(pathname);
		    log.debug(pathname);
		    log.debug("File exists: "+file.exists());
		    log.debug("File can be read: "+file.canRead());
		    
		    StringBuilder fileContents = new StringBuilder((int)file.length());
		    scanner = new Scanner(file);
		    String lineSeparator = System.getProperty("line.separator");

	        while(scanner.hasNextLine()) {
	            fileContents.append(scanner.nextLine() + lineSeparator);
	        }
	        fileContentAsString= fileContents.toString();
	    }catch(FileNotFoundException fileNotFoundException){
	    	log.error("File absolute path found: "+file.getAbsolutePath() +"\n"+"absolute path that sould be read: "+pathname);
	    	fileNotFoundException.printStackTrace();
	    }catch(IOException ioexception){
	    	ioexception.printStackTrace();
	    }
	    finally {
	        scanner.close();
	    }
		
		return fileContentAsString;
	}

}
