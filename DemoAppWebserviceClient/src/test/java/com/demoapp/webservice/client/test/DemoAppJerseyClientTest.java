package com.demoapp.webservice.client.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.webservice.client.error.DemoAppResponseError;
import com.demoapp.webservice.client.model.vo.ContactVO;
import com.demoapp.webservice.client.util.DemoAppWebserviceUrl;
import com.demoapp.webservice.client.util.EntryDataFormatType;
import com.demoapp.webservice.client.util.MediaTypeFileReader;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * 
 * 
 *
 */
public class DemoAppJerseyClientTest {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(DemoAppJerseyClientTest.class);

	/**
	 * 
	 */
	private ApplicationContext context;

	/**
	 * 
	 */
	private Client client;

	/**
	 * 
	 */
	private ContactVO contactVO;

	@Before
	public void setup() {
		this.context = new ClassPathXmlApplicationContext("SpringBeans.xml");
		this.contactVO = (ContactVO) context.getBean("contactVO");
		this.client = Client.create();
	}

	@Test
	public void findByName() throws Exception {

		try {
			WebResource webResource = client.resource(DemoAppWebserviceUrl.FIND_BY_NAME_URL);
			ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
			log.debug("Webservice Response - Find By Name: " + response);
			String output = response.getEntity(String.class);
			log.debug(output);
			assertNotNull(output);
			assertTrue(response.getStatus() == DemoAppResponseError.RESPONSE_GET_SUCCESS_CODE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void findByID() throws Exception {

		try {
			WebResource webResource = client.resource(DemoAppWebserviceUrl.FIND_BY_ID_URL);
			ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
			log.debug("Webservice Response - Find By ID: " + response);

			String output = response.getEntity(String.class);
			log.debug(output);
			assertNotNull(output);
			assertTrue(response.getStatus() == DemoAppResponseError.RESPONSE_GET_SUCCESS_CODE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @throws Exception
	 */

	@Test
	public void saveContactProvidedAsJSON() {

		try {
			WebResource webResource = this.client.resource(DemoAppWebserviceUrl.SAVE_CONTACT_URL);
			String jSONAsString = MediaTypeFileReader.readFile(DemoAppWebserviceUrl.JSON_FILE_URL);
			ClientResponse response = webResource.type(EntryDataFormatType.JSON_FORMAT).post(ClientResponse.class,
					jSONAsString);

			String output = response.getEntity(String.class);
			log.debug(response.getStatus());
			log.debug(output);
			assertNotNull(output);
			assertTrue(response.getStatus() == DemoAppResponseError.RESPONSE_POST_SUCCESS_CODE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void updateContactProvidedAsJSON() {

		try {
			WebResource webResource = this.client.resource(DemoAppWebserviceUrl.UPDATE_CONTACT_URL);
			String jSONAsString = MediaTypeFileReader.readFile(DemoAppWebserviceUrl.JSON_FILE_URL);
			ClientResponse response = webResource.type(EntryDataFormatType.JSON_FORMAT).post(ClientResponse.class,
					jSONAsString);

			String output = response.getEntity(String.class);
			log.debug(response.getStatus());
			log.debug(output);
			assertNotNull(output);
			assertTrue(response.getStatus() == DemoAppResponseError.RESPONSE_POST_SUCCESS_CODE);

			jSONAsString = MediaTypeFileReader.readFile(DemoAppWebserviceUrl.JSON_FILE_URL);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	@Test
	public void deleteContactProvidedAsJSON() {

		try {
			WebResource webResource = this.client.resource(DemoAppWebserviceUrl.UPDATE_CONTACT_URL);
			String jSONAsString = MediaTypeFileReader.readFile(DemoAppWebserviceUrl.JSON_FILE_URL);
			ClientResponse response = webResource.type(EntryDataFormatType.JSON_FORMAT).post(ClientResponse.class,
					jSONAsString);

			String output = response.getEntity(String.class);
			log.debug(response.getStatus());
			log.debug(output);
			assertNotNull(output);
			assertTrue(response.getStatus() == DemoAppResponseError.RESPONSE_POST_SUCCESS_CODE);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @return
	 */
	public ApplicationContext getContext() {
		return context;
	}

	/**
	 * 
	 * @param context
	 */
	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	/**
	 * 
	 * @return
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * 
	 * @param client
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 * 
	 * @return
	 */
	public ContactVO getContactVO() {
		return contactVO;
	}

	/**
	 * 
	 * @param contactVO
	 */
	public void setContactVO(ContactVO contactVO) {
		this.contactVO = contactVO;
	}

}
