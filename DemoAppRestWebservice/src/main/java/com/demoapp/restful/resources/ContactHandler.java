package com.demoapp.restful.resources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.backend.mongodb.entities.Contact;
import com.demoapp.backend.mongodb.service.ContactHandlerService;


@Path("/contacts")
public class ContactHandler {

	/**
	 * Exposes all services in a json format
	 */
	private ContactHandlerService contactHandlerService;

	/**
	 * 
	 */
	public ContactHandler() {
		ApplicationContext context = new ClassPathXmlApplicationContext("SpringBeans.xml");
		this.contactHandlerService = (ContactHandlerService) context.getBean("contactHandlerService");
	}

	/**
	 * URL to call the webservice: http://localhost:8080/DemoAppRestWebService/rest/contacts/findall
	 * @return
	 */
	@GET
	@Path("/findall")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<Contact> findAll() {
		List<Contact> contactList = null;
		try {
			this.contactHandlerService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contactList;
	}

	/**
	 * 
	 * @return
	 */
	public ContactHandlerService getContactHandlerService() {
		return contactHandlerService;
	}

	/**
	 * 
	 * @param contactHandlerService
	 */
	public void setContactHandlerService(ContactHandlerService contactHandlerService) {
		this.contactHandlerService = contactHandlerService;
	}

}
