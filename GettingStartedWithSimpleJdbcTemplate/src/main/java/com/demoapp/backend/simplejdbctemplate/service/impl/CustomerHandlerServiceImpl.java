package com.demoapp.backend.simplejdbctemplate.service.impl;

import java.util.List;

import com.demoapp.backend.simplejdbctemplate.converter.CustomerHandlerConverter;
import com.demoapp.backend.simplejdbctemplate.dao.CustomerHandlerDAO;
import com.demoapp.backend.simplejdbctemplate.entities.Customer;
import com.demoapp.backend.simplejdbctemplate.service.CustomerHandlerService;

/**
 * 
 * 
 *
 */
public class CustomerHandlerServiceImpl implements CustomerHandlerService{

	/**
	 * 
	 */
	private CustomerHandlerDAO customerHandlerDAO;
	
	/**
	 * 
	 */
	private CustomerHandlerConverter customerHandlerConverter;
	
	@Override
	public List<Customer> selectAllCustomers() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteCustomer(Customer customer) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateCustomer(Customer customer, String itemToBeUpdated) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void saveCustomer(Customer customer) throws Exception {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @return the customerHandlerDAO
	 */
	public CustomerHandlerDAO getCustomerHandlerDAO() {
		return customerHandlerDAO;
	}

	/**
	 * @param customerHandlerDAO the customerHandlerDAO to set
	 */
	public void setCustomerHandlerDAO(CustomerHandlerDAO customerHandlerDAO) {
		this.customerHandlerDAO = customerHandlerDAO;
	}

	/**
	 * @return the customerHandlerConverter
	 */
	public CustomerHandlerConverter getCustomerHandlerConverter() {
		return customerHandlerConverter;
	}

	/**
	 * @param customerHandlerConverter the customerHandlerConverter to set
	 */
	public void setCustomerHandlerConverter(CustomerHandlerConverter customerHandlerConverter) {
		this.customerHandlerConverter = customerHandlerConverter;
	}
	
	
	
}
