package com.demoapp.backend.simplejdbctemplate.converter;

import com.demoapp.backend.simplejdbctemplate.entities.Customer;
import com.demoapp.backend.simplejdbctemplate.model.to.CustomerTO;
import com.demoapp.backend.simplejdbctemplate.model.vo.CustomerVO;

public class CustomerHandlerConverter {

	/**
	 * 
	 * @param contact
	 * @return ContactTO
	 * @throws Exception
	 */
	public CustomerTO toContactTO(Customer customer) throws Exception {

		CustomerTO customerTO = new CustomerTO();

		customerTO.setCity(customer.getCity());
		customerTO.setCreditLimit(customer.getCreditLimit());
		customerTO.setCustomerEmail(customer.getCustomerEmail());
		customerTO.setCustomerId(customer.getCustomerId());
		customerTO.setCustomerStreetAddress1(customer.getCustomerStreetAddress1());
		customerTO.setCustomerStreetAddress2(customer.getCustomerStreetAddress2());
		customerTO.setFirstName(customer.getFirstName());
		customerTO.setLastName(customer.getLastName());
		customerTO.setPhoneNumber1(customer.getPhoneNumber1());
		customerTO.setPhoneNumber2(customer.getPhoneNumber2());
		
		return customerTO;
	}

	/**
	 * 
	 * @param CustomerVO
	 * @return
	 * @throws Exception
	 */
	public Customer fromVOtoContactEntity(CustomerVO customerVO) throws Exception {
		Customer customer = new Customer();

		customer.setCity(customerVO.getCity());
		customer.setCreditLimit(customerVO.getCreditLimit());
		customer.setCustomerEmail(customerVO.getCustomerEmail());
		customer.setCustomerId(customerVO.getCustomerId());
		customer.setCustomerStreetAddress1(customerVO.getCustomerStreetAddress1());
		customer.setCustomerStreetAddress2(customerVO.getCustomerStreetAddress2());
		customer.setFirstName(customerVO.getFirstName());
		customer.setLastName(customerVO.getLastName());
		customer.setPhoneNumber1(customerVO.getPhoneNumber1());
		customer.setPhoneNumber2(customerVO.getPhoneNumber2());
		
		return customer;
	}

	/**
	 * 
	 * @param contactTO
	 * @return
	 * @throws Exception
	 */
	public Customer fromTOtoContactEntity(CustomerTO customerTO) throws Exception {

		Customer customer = new Customer();

		customer.setCity(customerTO.getCity());
		customer.setCreditLimit(customerTO.getCreditLimit());
		customer.setCustomerEmail(customerTO.getCustomerEmail());
		customer.setCustomerId(customerTO.getCustomerId());
		customer.setCustomerStreetAddress1(customerTO.getCustomerStreetAddress1());
		customer.setCustomerStreetAddress2(customerTO.getCustomerStreetAddress2());
		customer.setFirstName(customerTO.getFirstName());
		customer.setLastName(customerTO.getLastName());
		customer.setPhoneNumber1(customerTO.getPhoneNumber1());
		customer.setPhoneNumber2(customerTO.getPhoneNumber2());
		
		return customer;
		
	}

}
