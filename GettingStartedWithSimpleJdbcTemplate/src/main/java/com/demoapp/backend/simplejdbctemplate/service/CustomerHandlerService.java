package com.demoapp.backend.simplejdbctemplate.service;

import java.util.List;

import com.demoapp.backend.simplejdbctemplate.entities.Customer;


/**
 * 
 * 
 *
 */
public interface CustomerHandlerService {

	/**
	 * 
	 * @return List<Customer>
	 * @throws Exception
	 */
	public List<Customer> selectAllCustomers()throws Exception;
	
	/**
	 * Delete customer
	 * @param customer
	 * @return rows affected
	 * @throws Exception
	 */
	public int deleteCustomer(Customer customer)throws Exception;
	
	/**
	 * Update Customer details
	 * @param customer
	 * @param itemToBeUpdated
	 * @return affected rows
	 * @throws Exception
	 */
	public int updateCustomer(Customer customer, String itemToBeUpdated)throws Exception;
	
	
	/**
	 * 
	 * @param customer
	 * @throws Exception
	 */
	public void saveCustomer(Customer customer)throws Exception;
	 

}
