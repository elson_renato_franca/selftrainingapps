package com.demoapp.backend.simplejdbctemplate.dao;

import java.util.List;

import com.demoapp.backend.simplejdbctemplate.entities.Customer;

/**
 * 
 * 
 *
 */
public interface CustomerHandlerDAO {


	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Customer> selectAllCustomers()throws Exception;
	
	/**
	 * Delete customer
	 * @param customer
	 * @return rows affected
	 * @throws Exception
	 */
	public int deleteCustomer(Customer customer)throws Exception;
	
	/**
	 * 
	 * @param customer
	 * @throws Exception
	 */
	public void saveCustomer(Customer customer)throws Exception;
	
	/**
	 * 
	 * @param customer
	 * @return
	 * @throws Exception
	 */
	public int updateCustomer(Customer customer)throws Exception;
}
