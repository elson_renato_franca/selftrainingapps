package com.demoapp.backend.simplejdbctemplate.entities;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * 
 *
 */
public class Customer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	private int customerId;
	
	/**
	 * 
	 */
	private String firstName;
	
	/**
	 * 
	 */
	private String lastName;
	
	/**
	 * 
	 */
	private String customerStreetAddress1;
	
	/**
	 * 
	 */
	private String customerStreetAddress2;
	
	/**
	 * 
	 */
	private String city;
	
	/**
	 * 
	 */
	private String state;
	
	/**
	 * 
	 */
	private String postalCode;
	
	/**
	 * 
	 */
	private String phoneNumber1;
	
	/**
	 * 
	 */
	private String phoneNumber2;
	
	/**
	 * 
	 */
	private BigDecimal creditLimit;
	
	/**
	 * 
	 */
	private String customerEmail;

	/**
	 * @return the customerId
	 */
	public int getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the customerStreetAddress1
	 */
	public String getCustomerStreetAddress1() {
		return customerStreetAddress1;
	}

	/**
	 * @param customerStreetAddress1 the customerStreetAddress1 to set
	 */
	public void setCustomerStreetAddress1(String customerStreetAddress1) {
		this.customerStreetAddress1 = customerStreetAddress1;
	}

	/**
	 * @return the customerStreetAddress2
	 */
	public String getCustomerStreetAddress2() {
		return customerStreetAddress2;
	}

	/**
	 * @param customerStreetAddress2 the customerStreetAddress2 to set
	 */
	public void setCustomerStreetAddress2(String customerStreetAddress2) {
		this.customerStreetAddress2 = customerStreetAddress2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the phoneNumber1
	 */
	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	/**
	 * @param phoneNumber1 the phoneNumber1 to set
	 */
	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	/**
	 * @return the phoneNumber2
	 */
	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	/**
	 * @param phoneNumber2 the phoneNumber2 to set
	 */
	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	/**
	 * @return the creditLimit
	 */
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	/**
	 * @param creditLimit the creditLimit to set
	 */
	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	/**
	 * @return the customerEmail
	 */
	public String getCustomerEmail() {
		return customerEmail;
	}

	/**
	 * @param customerEmail the customerEmail to set
	 */
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((creditLimit == null) ? 0 : creditLimit.hashCode());
		result = prime * result + ((customerEmail == null) ? 0 : customerEmail.hashCode());
		result = prime * result + customerId;
		result = prime * result + ((customerStreetAddress1 == null) ? 0 : customerStreetAddress1.hashCode());
		result = prime * result + ((customerStreetAddress2 == null) ? 0 : customerStreetAddress2.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phoneNumber1 == null) ? 0 : phoneNumber1.hashCode());
		result = prime * result + ((phoneNumber2 == null) ? 0 : phoneNumber2.hashCode());
		result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (creditLimit == null) {
			if (other.creditLimit != null)
				return false;
		} else if (!creditLimit.equals(other.creditLimit))
			return false;
		if (customerEmail == null) {
			if (other.customerEmail != null)
				return false;
		} else if (!customerEmail.equals(other.customerEmail))
			return false;
		if (customerId != other.customerId)
			return false;
		if (customerStreetAddress1 == null) {
			if (other.customerStreetAddress1 != null)
				return false;
		} else if (!customerStreetAddress1.equals(other.customerStreetAddress1))
			return false;
		if (customerStreetAddress2 == null) {
			if (other.customerStreetAddress2 != null)
				return false;
		} else if (!customerStreetAddress2.equals(other.customerStreetAddress2))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phoneNumber1 == null) {
			if (other.phoneNumber1 != null)
				return false;
		} else if (!phoneNumber1.equals(other.phoneNumber1))
			return false;
		if (phoneNumber2 == null) {
			if (other.phoneNumber2 != null)
				return false;
		} else if (!phoneNumber2.equals(other.phoneNumber2))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", customerStreetAddress1=" + customerStreetAddress1 + ", customerStreetAddress2="
				+ customerStreetAddress2 + ", city=" + city + ", state=" + state + ", postalCode=" + postalCode
				+ ", phoneNumber1=" + phoneNumber1 + ", phoneNumber2=" + phoneNumber2 + ", creditLimit=" + creditLimit
				+ ", customerEmail=" + customerEmail + "]";
	}

	
	

}
