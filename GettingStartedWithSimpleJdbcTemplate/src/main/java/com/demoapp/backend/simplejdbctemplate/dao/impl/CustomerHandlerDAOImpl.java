package com.demoapp.backend.simplejdbctemplate.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.demoapp.backend.simplejdbctemplate.dao.CustomerHandlerDAO;
import com.demoapp.backend.simplejdbctemplate.entities.Customer;

/**
 * 
 * 
 *
 */
public class CustomerHandlerDAOImpl implements CustomerHandlerDAO {

	/**
	 * 
	 */
	private DataSource dataSource;

	/**
	 * 
	 */
	private JdbcTemplate jdbcTemplateObject;
	
	

	@Override
	public List<Customer> selectAllCustomers() throws Exception {
		String SQL = "select * from Student";
		// List<Customer> customerList = jdbcTemplateObject.query(SQL,
		// new CustomerMapper());
		return null;
	}

	@Override
	public int deleteCustomer(Customer customer) throws Exception {
		String SQL = "delete from Student where id = ?";
		// jdbcTemplateObject.update(SQL, id);
		// System.out.println("Deleted Record with ID = " + id);

		return 0;
	}

	@Override
	public void saveCustomer(Customer customer) throws Exception {
		String SQL = "insert into Customer (name, age) values (?, ?)";

		//jdbcTemplateObject.update(SQL, name, age);
		//System.out.println("Created Record Name = " + name + " Age = " + age);
		//return;

	}

	@Override
	public int updateCustomer(Customer customer) throws Exception {
		String SQL = "update Student set age = ? where id = ?";
		// jdbcTemplateObject.update(SQL, age, id);
		// System.out.println("Updated Record with ID = " + id);
		return 0;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	}

}
