package com.demoapp.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.demoapp.dao.CustomerHandlerDAO;
import com.demoapp.entities.Customer;
import com.demoapp.util.to.CustomerDetailsTO;
import com.demoapp.util.vo.CustomerDetailsVO;

/**
 * 
 * 
 *
 */
public class CustomerHandlerDAOImpl implements CustomerHandlerDAO {

	/**
	 * 
	 */
	private EntityManager entityManager;

	/**
	 * 
	 */
	private EntityManagerFactory entityManagerFactory;

	/**
	 * Add a new customer to database
	 * 
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public CustomerDetailsTO addCustomer(CustomerDetailsVO customerDetailsVO) throws Exception {

		CustomerDetailsTO customerDetailsTO = null;

		try {
			this.getEntityManager().getTransaction().begin();
			Customer customer = this.findCustomer(customerDetailsVO);
			if (customer.getCustomerId() == null) {
				this.getEntityManager().persist(customerDetailsVO);
			} else {
				this.getEntityManager().merge(customerDetailsVO);
			}
			this.getEntityManager().getTransaction().commit();
		} finally {
			this.getEntityManager().close();
		}
		return customerDetailsTO;
	}

	/**
	 * Delete an existing customer
	 * 
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public void deleteCustomer(CustomerDetailsVO customerDetailsVO) throws Exception {
		try {
			Customer customer = findCustomer(customerDetailsVO);
			this.getEntityManager().getTransaction().begin();
			this.getEntityManager().remove(customer);
			this.getEntityManager().getTransaction().commit();
		} finally {
			this.getEntityManager().close();
		}


	}

	/**
	 * 
	 * @param customerDetailsVO
	 * @return
	 * @throws Exception
	 */
	public CustomerDetailsTO searchCustomerById(CustomerDetailsVO customerDetailsVO) throws Exception {

		CustomerDetailsTO customerDetailsTO = null;

		try {
			Customer customer = findCustomer(customerDetailsVO);
			customerDetailsTO = new CustomerDetailsTO();
			customerDetailsTO.setCity(customer.getCity());
			customerDetailsTO.setFirstName(customer.getFirstName());
			customerDetailsTO.setLastName(customer.getLastName());

		} finally {
			this.getEntityManager().close();
		}

		return customerDetailsTO;
	}
	

	/**
	 * Update a customer
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public void updateCustomer(CustomerDetailsVO customerDetailsVO) throws Exception {
		
		try {
			Customer customer = findCustomer(customerDetailsVO);
			this.getEntityManager().getTransaction().begin();
			customer.setCity(customerDetailsVO.getCity());
			customer.setFirstName(customerDetailsVO.getFirstName());
			this.getEntityManager().getTransaction().commit();
			
		} finally {
			this.getEntityManager().close();
		}


	}

	/**
	 * Search for all customers
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<CustomerDetailsTO> searchAllCustomers() throws Exception {
		
		List<CustomerDetailsTO> customerDetailsTOList = null;
		
		List<Customer> customerList = this.getEntityManager()
				.createQuery("Select c From Customer c", Customer.class).getResultList();
		
		if(customerList !=null && !customerList.isEmpty()){
			customerDetailsTOList = new ArrayList<CustomerDetailsTO>();
			for(int i=0; i< customerList.size(); i++){
				CustomerDetailsTO customerDetailsTO = new CustomerDetailsTO(); 
				Customer customer = customerList.get(i);
				customerDetailsTO.setCity(customer.getCity());
				customerDetailsTO.setCreditLimit(customer.getCreditLimit());
				customerDetailsTO.setCustomerId(customer.getCustomerId());
				customerDetailsTO.setCustomerEmail(customer.getCustomerEmail());
				customerDetailsTO.setCustomerStreetAddress1(customer.getCustomerStreetAddress1());
				customerDetailsTO.setCustomerStreetAddress2(customer.getCustomerStreetAddress2());
				customerDetailsTO.setFirstName(customer.getFirstName());
				customerDetailsTO.setLastName(customer.getLastName());
				customerDetailsTO.setPhoneNumber1(customer.getPhoneNumber1());
				customerDetailsTO.setPhoneNumber2(customer.getPhoneNumber2());
				customerDetailsTO.setPostalCode(customer.getPostalCode());
				customerDetailsTOList.add(customerDetailsTO);
			}
		}
		
		return customerDetailsTOList;
	}
	
	
	/**
	 * 
	 * @param customerDetailsVO
	 * @return
	 * @throws Exception
	 */
	private Customer findCustomer(CustomerDetailsVO customerDetailsVO)throws Exception{
		Customer customer = this.getEntityManager().find(Customer.class, customerDetailsVO.getCustomerId()); 
		return customer;
	}


	/**
	 * 
	 * @return
	 */
	public EntityManager getEntityManager() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("myPersistentUnit");
		this.entityManager = this.entityManagerFactory.createEntityManager();
	
		return entityManager;
	}

	/**
	 * 
	 * @param entityManager
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * 
	 * @return
	 */
	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	/**
	 * 
	 * @param entityManagerFactory
	 */
	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}


}
