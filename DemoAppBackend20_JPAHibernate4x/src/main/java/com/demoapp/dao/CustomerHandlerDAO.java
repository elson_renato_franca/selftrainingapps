package com.demoapp.dao;

import java.util.List;

import com.demoapp.util.to.CustomerDetailsTO;
import com.demoapp.util.vo.CustomerDetailsVO;

/**
 * 
 * 
 *
 */
public interface CustomerHandlerDAO {

	/**
	 * Search customer by Id
	 * @param customerDetailsVO
	 * @return
	 * @throws Exception
	 */
	public CustomerDetailsTO searchCustomerById(CustomerDetailsVO customerDetailsVO) throws Exception;
	
	
	/**
	 * Add a new customer to database
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public CustomerDetailsTO addCustomer(CustomerDetailsVO customerDetailsVO) throws Exception;
	
	/**
	 * Delete an existing customer
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public void deleteCustomer(CustomerDetailsVO customerDetailsVO) throws Exception;
	
	
	/**
	 * Update a customer
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public void updateCustomer(CustomerDetailsVO customerDetailsVO)throws Exception;
	
	
	/**
	 * Search for all customers
	 * @return
	 * @throws Exception
	 */
	public List<CustomerDetailsTO> searchAllCustomers()throws Exception;
	

}
