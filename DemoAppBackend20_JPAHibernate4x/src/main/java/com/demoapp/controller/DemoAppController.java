package com.demoapp.controller;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.services.CustomerHandlerService;
import com.demoapp.util.to.CustomerDetailsTO;
import com.demoapp.util.vo.CustomerDetailsVO;

public class DemoAppController {

	public static void main(String[] args){
		
		try {
			ApplicationContext context = new ClassPathXmlApplicationContext("DemoAppBean.xml");
			CustomerHandlerService customerHandlerService= (CustomerHandlerService)context.getBean("customerHandlerService");
			CustomerDetailsVO customerDetailsVO = (CustomerDetailsVO)context.getBean("customerDetailsVO");
			CustomerDetailsTO customerDetailsTO = customerHandlerService.searchCustomerById(customerDetailsVO);
			List<CustomerDetailsTO> customerList = customerHandlerService.searchAllCustomers();
			
			if(customerDetailsTO !=null){
				System.out.println(customerDetailsTO.getFirstName());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
}
