package com.demoapp.spring.basics.collections.test.suit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.demoapp.spring.basics.collections.test.SpringCollectionsTest;
import com.demoapp.spring.basics.constructor.test.ConstructorInjectionTest;

@RunWith(Suite.class)
@SuiteClasses({ SpringCollectionsTest.class, ConstructorInjectionTest.class })
public class AllTestsSuit {

}
