package com.demoapp.spring.basics.constructor.test;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.spring.basics.constructorinjection.ConstructorMessage;
import com.demoapp.spring.basics.util.SpringBasicsUtils;

/**
 * 
 * 
 *
 */
public class ConstructorInjectionTest {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(ConstructorInjectionTest.class);

	/**
	 * 
	 */
	private ApplicationContext applicationContext;

	
	/**
	 * 
	 */
	private ConstructorMessage constructorMessage;
	
	@Before
	public void setup() {
		this.applicationContext = new ClassPathXmlApplicationContext(SpringBasicsUtils.FILE_PATH);
		this.constructorMessage =(ConstructorMessage)applicationContext.getBean("constructorMessage");
	}
	
	/**
	 * 
	 */
	@Test
	public void checkMessageContent(){
		log.debug(constructorMessage);
		assertNotNull(constructorMessage);
	}
	
	@After
	public void tearDown(){
		
	}

	/**
	 * @return the applicationContext
	 */
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * @param applicationContext the applicationContext to set
	 */
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	/**
	 * @return the constructorMessage
	 */
	public ConstructorMessage getConstructorMessage() {
		return constructorMessage;
	}

	/**
	 * @param constructorMessage the constructorMessage to set
	 */
	public void setConstructorMessage(ConstructorMessage constructorMessage) {
		this.constructorMessage = constructorMessage;
	}

	
}
