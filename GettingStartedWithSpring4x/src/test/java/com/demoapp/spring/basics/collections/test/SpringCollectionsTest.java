package com.demoapp.spring.basics.collections.test;

import static org.junit.Assert.assertNotNull;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.spring.basics.collections.SpringBasicsCollections;
import com.demoapp.spring.basics.util.SpringBasicsUtils;

/**
 * 
 * 
 *
 */
public class SpringCollectionsTest {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(SpringCollectionsTest.class);

	/**
	 * 
	 */
	private ApplicationContext applicationContext;

	/**
	 * 
	 */
	private SpringBasicsCollections springCollections;

	@Before
	public void setup() {
		this.applicationContext = new ClassPathXmlApplicationContext(SpringBasicsUtils.FILE_PATH);
		this.springCollections = (SpringBasicsCollections) applicationContext.getBean("javaCollection");
	}

	@Test
	public void showListForEach() {
		List<Object> objectList = this.springCollections.getObjectList();

		if (objectList != null) {
			// Iterates a list using the foreach command
			for (Object object : objectList) {
				log.debug(object);
				assertNotNull(object);
			}
		}

	}

	@Test
	public void showListIterator() {
		List<Object> objectList = this.springCollections.getObjectList();

		// Iterates the list using Iterator semantics
		Iterator<Object> iterator = objectList.iterator();

		if (iterator != null) {
			while (iterator.hasNext()) {
				log.debug(iterator.next());
				assertNotNull(iterator.next());
			}

		}

		assertNotNull(objectList);
	}

	@Test
	public void orderList() {

		List<Object> objectList = this.springCollections.getObjectList();

		// Iterates the list using Iterator semantics
		Iterator<Object> iterator = objectList.iterator();

		if (iterator != null) {
			while (iterator.hasNext()) {
				log.debug(iterator.next());
				assertNotNull(iterator.next());
			}

		}

		assertNotNull(objectList);

	}

	@Test
	public void shownSet() {
		Set<Object> objectSet = this.springCollections.getObjectSet();
		assertNotNull(objectSet);
	}

	@Test
	public void objectMap() {
		Map<Object, Object> objectMap = this.springCollections.getObjectMap();
		log.debug(objectMap);
		assertNotNull(objectMap);
	}

	@Test
	public void objectProperties() {
		Properties objectProperties = this.springCollections.getObjectProp();
		log.debug(objectProperties);
		assertNotNull(objectProperties);
	}

	@After
	public void tearDown() {
		this.applicationContext = null;
		this.springCollections = null;
	}

	/**
	 * @return the applicationContext
	 */
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * @param applicationContext
	 *            the applicationContext to set
	 */
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	/**
	 * @return the springCollections
	 */
	public SpringBasicsCollections getSpringCollections() {
		return springCollections;
	}

	/**
	 * @param springCollections
	 *            the springCollections to set
	 */
	public void setSpringCollections(SpringBasicsCollections springCollections) {
		this.springCollections = springCollections;
	}

	
	
}
