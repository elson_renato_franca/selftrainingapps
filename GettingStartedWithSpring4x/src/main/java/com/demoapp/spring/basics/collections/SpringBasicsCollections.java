package com.demoapp.spring.basics.collections;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * 
 * 
 *
 */
public class SpringBasicsCollections implements Comparable<Object>{

	/**
	 * 
	 */
	private List<Object> objectList;

	/**
	 * A Set is a Collection that cannot contain duplicate elements. It models
	 * the mathematical set abstraction.
	 */
	private Set<Object> objectSet;
	
	

	/**
	 * 
	 */
	private Map<Object, Object> objectMap;

	/**
	 * 
	 */
	private Properties objectProp;

	/**
	 * @return the objectList
	 */
	public List<Object> getObjectList() {
		return objectList;
	}

	/**
	 * @param objectList the objectList to set
	 */
	public void setObjectList(List<Object> objectList) {
		this.objectList = objectList;
	}

	/**
	 * @return the objectSet
	 */
	public Set<Object> getObjectSet() {
		return objectSet;
	}

	/**
	 * @param objectSet the objectSet to set
	 */
	public void setObjectSet(Set<Object> objectSet) {
		this.objectSet = objectSet;
	}

	/**
	 * @return the objectMap
	 */
	public Map<Object, Object> getObjectMap() {
		return objectMap;
	}

	/**
	 * @param objectMap the objectMap to set
	 */
	public void setObjectMap(Map<Object, Object> objectMap) {
		this.objectMap = objectMap;
	}

	/**
	 * @return the objectProp
	 */
	public Properties getObjectProp() {
		return objectProp;
	}

	/**
	 * @param objectProp the objectProp to set
	 */
	public void setObjectProp(Properties objectProp) {
		this.objectProp = objectProp;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objectList == null) ? 0 : objectList.hashCode());
		result = prime * result + ((objectMap == null) ? 0 : objectMap.hashCode());
		result = prime * result + ((objectProp == null) ? 0 : objectProp.hashCode());
		result = prime * result + ((objectSet == null) ? 0 : objectSet.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpringBasicsCollections other = (SpringBasicsCollections) obj;
		if (objectList == null) {
			if (other.objectList != null)
				return false;
		} else if (!objectList.equals(other.objectList))
			return false;
		if (objectMap == null) {
			if (other.objectMap != null)
				return false;
		} else if (!objectMap.equals(other.objectMap))
			return false;
		if (objectProp == null) {
			if (other.objectProp != null)
				return false;
		} else if (!objectProp.equals(other.objectProp))
			return false;
		if (objectSet == null) {
			if (other.objectSet != null)
				return false;
		} else if (!objectSet.equals(other.objectSet))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SpringBasicsCollections [objectList=" + objectList + ", objectSet=" + objectSet + ", objectMap="
				+ objectMap + ", objectProp=" + objectProp + "]";
	}

	@Override
	public int compareTo(Object anotherObject) {
		
		if(this.objectList !=null){
			
			// Iterates the list using Iterator semantics
			Iterator<Object> iterator =this.objectList.iterator();

			if (iterator != null) {
				while (iterator.hasNext()) {
					String object = (String)iterator.next();
				}

			}


		}
		

	        return 0;
	}

	
	
}
