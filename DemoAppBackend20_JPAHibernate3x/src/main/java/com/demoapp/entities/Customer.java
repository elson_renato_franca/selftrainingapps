
package com.demoapp.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DEMO_CUSTOMERS")
public class Customer {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CUSTOMER_ID")
	private Long customerId;
	
	@Column(name="CUST_FIRST_NAME")
	private String firstName;
	
	@Column(name="CUST_LAST_NAME")
	private String lastName;
	
	@Column(name="CUST_STREET_ADDRESS1")
	private String customerStreetAddress1;
	
	@Column(name="CUST_STREET_ADDRESS2")
	private String customerStreetAddress2;
	
	@Column(name="CUST_CITY")
	private String city;
	
	@Column(name="CUST_STATE")
	private String state;
	
	@Column(name="CUST_POSTAL_CODE")
	private String postalCode;
	
	@Column(name="PHONE_NUMBER1")
	private String phoneNumber1;
	
	@Column(name="PHONE_NUMBER2")
	private String phoneNumber2;
	
	@Column(name="CREDIT_LIMIT")
	private BigDecimal creditLimit;
	
	@Column(name="CUST_EMAIL")
	private String customerEmail;

	/**
	 * 
	 * @return
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * 
	 * @param customerId
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	/**
	 * 
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * 
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * 
	 * @return
	 */
	public String getCustomerStreetAddress1() {
		return customerStreetAddress1;
	}

	/**
	 * 
	 * @param customerStreetAddress1
	 */
	public void setCustomerStreetAddress1(String customerStreetAddress1) {
		this.customerStreetAddress1 = customerStreetAddress1;
	}

	/**
	 * 
	 * @return
	 */
	public String getCustomerStreetAddress2() {
		return customerStreetAddress2;
	}

	/**
	 * 
	 * @param customerStreetAddress2
	 */
	public void setCustomerStreetAddress2(String customerStreetAddress2) {
		this.customerStreetAddress2 = customerStreetAddress2;
	}

	/**
	 * 
	 * @return
	 */
	public String getCity() {
		return city;
	}

	/**
	 * 
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * 
	 * @return
	 */
	public String getState() {
		return state;
	}

	/**
	 * 
	 * @param state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 
	 * @return
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * 
	 * @param postalCode
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * 
	 * @return
	 */
	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	/**
	 * 
	 * @param phoneNumber1
	 */
	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	/**
	 * 
	 * @return
	 */
	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	/**
	 * 
	 * @param phoneNumber2
	 */
	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	/**
	 * 
	 * @param creditLimit
	 */
	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	/**
	 * 
	 * @return
	 */
	public String getCustomerEmail() {
		return customerEmail;
	}

	/**
	 * 
	 * @param customerEmail
	 */
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	
	
}
