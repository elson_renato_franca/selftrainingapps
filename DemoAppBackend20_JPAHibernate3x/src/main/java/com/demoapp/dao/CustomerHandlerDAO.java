package com.demoapp.dao;

import com.demoapp.util.to.CustomerDetailsTO;

/**
 * 
 * 
 *
 */
public interface CustomerHandlerDAO {

	/**
	 * Search customer by Id
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public CustomerDetailsTO searchCustomerById(Long id) throws Exception;
	
	
}
