package com.demoapp.services.impl;

import com.demoapp.dao.CustomerHandlerDAO;
import com.demoapp.services.CustomerHandlerService;
import com.demoapp.util.to.CustomerDetailsTO;
import com.demoapp.util.vo.CustomerDetailsVO;

/**
 * 
 * 
 *
 */
public class CustomerHandlerServiceImpl implements CustomerHandlerService{

	/**
	 * 
	 */
	private CustomerHandlerDAO customerHandlerDAO;
	

	/**
	 * Add a new customer to database
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public void addCustomer(CustomerDetailsVO customerDetailsVO) throws Exception {
		// TODO Auto-generated method stub
		
	}


	/**
	 * Delete an existing customer
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public void deleteCustomer(CustomerDetailsVO customerDetailsVO) throws Exception {
		// TODO Auto-generated method stub
		
	}


	/**
	 * Search customer by Id
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public CustomerDetailsTO searchCustomerById(Long id) throws Exception {
		return customerHandlerDAO.searchCustomerById(id);
	}


	/**
	 * 
	 * @return
	 */
	public CustomerHandlerDAO getCustomerHandlerDAO() {
		return customerHandlerDAO;
	}


	/**
	 * 
	 * @param customerHandlerDAO
	 */
	public void setCustomerHandlerDAO(CustomerHandlerDAO customerHandlerDAO) {
		this.customerHandlerDAO = customerHandlerDAO;
	}

}
