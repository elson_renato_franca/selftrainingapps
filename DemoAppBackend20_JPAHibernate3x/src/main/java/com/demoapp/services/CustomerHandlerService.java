package com.demoapp.services;

import com.demoapp.util.to.CustomerDetailsTO;
import com.demoapp.util.vo.CustomerDetailsVO;

/**
 * 
 * 
 *
 */
public interface CustomerHandlerService {

	/**
	 * Add a new customer to database
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public void addCustomer(CustomerDetailsVO customerDetailsVO) throws Exception;
	
	/**
	 * Delete an existing customer
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public void deleteCustomer(CustomerDetailsVO customerDetailsVO) throws Exception;
	
	/**
	 * Search customer by Id
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public CustomerDetailsTO searchCustomerById(Long id) throws Exception;
	
}
