package com.demoapp.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * This class is responsible for mapping to EMP table
 *
 */
public class Employee implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private BigDecimal employeeNumber;
	
	/**
	 * 
	 */
	private String employeeName;
	
	/**
	 * 
	 */
	private String employeeJob;
	
	/**
	 * 
	 */
	private BigDecimal employeeMgr;
	
	/**
	 * 
	 */
	private Date employeeHireDate;
	
	/**
	 * 
	 */
	private BigDecimal employeeSalary;
	
	/**
	 * 
	 */
	private BigDecimal employeeComm;
	
	/**
	 * 
	 */
	private Department departmentNumber;

	/**
	 * 
	 * @return
	 */
	public BigDecimal getEmployeeNumber() {
		return employeeNumber;
	}

	/**
	 * 
	 * @param employeeNumber
	 */
	public void setEmployeeNumber(BigDecimal employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeName() {
		return employeeName;
	}

	/**
	 * 
	 * @param employeeName
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmployeeJob() {
		return employeeJob;
	}

	/**
	 * 
	 * @param employeeJob
	 */
	public void setEmployeeJob(String employeeJob) {
		this.employeeJob = employeeJob;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getEmployeeMgr() {
		return employeeMgr;
	}

	/**
	 * 
	 * @param employeeMgr
	 */
	public void setEmployeeMgr(BigDecimal employeeMgr) {
		this.employeeMgr = employeeMgr;
	}

	/**
	 * 
	 * @return
	 */
	public Date getEmployeeHireDate() {
		return employeeHireDate;
	}

	/**
	 * 
	 * @param employeeHireDate
	 */
	public void setEmployeeHireDate(Date employeeHireDate) {
		this.employeeHireDate = employeeHireDate;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getEmployeeSalary() {
		return employeeSalary;
	}

	/**
	 * 
	 * @param employeeSalary
	 */
	public void setEmployeeSalary(BigDecimal employeeSalary) {
		this.employeeSalary = employeeSalary;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getEmployeeComm() {
		return employeeComm;
	}

	/**
	 * 
	 * @param employeeComm
	 */
	public void setEmployeeComm(BigDecimal employeeComm) {
		this.employeeComm = employeeComm;
	}

	/**
	 * 
	 * @return
	 */
	public Department getDepartmentNumber() {
		return departmentNumber;
	}

	/**
	 * 
	 * @param departmentNumber
	 */
	public void setDepartmentNumber(Department departmentNumber) {
		this.departmentNumber = departmentNumber;
	}
	
	
}
