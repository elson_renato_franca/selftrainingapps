package com.demoapp.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * 
 *
 */
public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private BigDecimal userId;
	
	/**
	 * 
	 */
	private String userName;
	
	/**
	 * 
	 */
	private String userPassword;
	
	/**
	 * 
	 */
	private Date userCreatedOn;
	
	/**
	 * 
	 */
	private BigDecimal userQuota;
	
	/**
	 * 
	 */
	private String userProducts;
	
	/**
	 * 
	 */
	private Date userExpiresOn;
	
	/**
	 * 
	 */
	private String userAdmin;

	/**
	 * 
	 * @return
	 */
	public BigDecimal getUserId() {
		return userId;
	}

	/**
	 * 
	 * @param userId
	 */
	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}

	/**
	 * 
	 * @return
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * 
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 
	 * @return
	 */
	public String getUserPassword() {
		return userPassword;
	}

	/**
	 * 
	 * @param userPassword
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	/**
	 * 
	 * @return
	 */
	public Date getUserCreatedOn() {
		return userCreatedOn;
	}

	/**
	 * 
	 * @param userCreatedOn
	 */
	public void setUserCreatedOn(Date userCreatedOn) {
		this.userCreatedOn = userCreatedOn;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getUserQuota() {
		return userQuota;
	}

	/**
	 * 
	 * @param userQuota
	 */
	public void setUserQuota(BigDecimal userQuota) {
		this.userQuota = userQuota;
	}

	/**
	 * 
	 * @return
	 */
	public String getUserProducts() {
		return userProducts;
	}

	/**
	 * 
	 * @param userProducts
	 */
	public void setUserProducts(String userProducts) {
		this.userProducts = userProducts;
	}

	/**
	 * 
	 * @return
	 */
	public Date getUserExpiresOn() {
		return userExpiresOn;
	}

	/**
	 * 
	 * @param userExpiresOn
	 */
	public void setUserExpiresOn(Date userExpiresOn) {
		this.userExpiresOn = userExpiresOn;
	}

	/**
	 * 
	 * @return
	 */
	public String getUserAdmin() {
		return userAdmin;
	}

	/**
	 * 
	 * @param userAdmin
	 */
	public void setUserAdmin(String userAdmin) {
		this.userAdmin = userAdmin;
	}
	
	
}
