package com.demoapp.entities;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * Class responsible for mapping table DEPT
 *
 */
public class Department implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Map DEPTNO Number(2,0) to Java.lang.BigDecimal
	 */
	private BigDecimal departmentNumber;
	
	/**
	 * Map DNAME Varchar2(14) to string
	 */
	private String departmentName;
	
	/**
	 * Map LOC Varchar2(13) to string
	 */
	private String departmentLocation;

	/**
	 * 
	 * @return
	 */
	public BigDecimal getDepartmentNumber() {
		return departmentNumber;
	}

	/**
	 * 
	 * @param departmentNumber
	 */
	public void setDepartmentNumber(BigDecimal departmentNumber) {
		this.departmentNumber = departmentNumber;
	}

	/**
	 * 
	 * @return
	 */
	public String getDepartmentName() {
		return departmentName;
	}

	/**
	 * 
	 * @param departmentName
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * 
	 * @return
	 */
	public String getDepartmentLocation() {
		return departmentLocation;
	}

	/**
	 * 
	 * @param departmentLocation
	 */
	public void setDepartmentLocation(String departmentLocation) {
		this.departmentLocation = departmentLocation;
	}
	
		
}
