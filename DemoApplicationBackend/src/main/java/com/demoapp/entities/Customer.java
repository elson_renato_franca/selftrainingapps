package com.demoapp.entities;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * Class responsible for mapping table DEMO_CUSTOMERS
 *
 */
public class Customer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	/**
	 * 
	 */
	private int customerId;
	
	/**
	 * 
	 */
	private String firstName;
	
	/**
	 * 
	 */
	private String lastName;
	
	/**
	 * 
	 */
	private String lastAddress1;
	
	/**
	 * 
	 */
	private String lastAddress2;
	
	/**
	 * 
	 */
	private String city;
	
	/**
	 * 
	 */
	private String state;
	
	/**
	 * 
	 */
	private String postalCode;
	
	/**
	 * 
	 */
	private String phoneNumber1;
	
	/**
	 * 
	 */
	private String phoneNumber2;
	
	/**
	 * 
	 */
	private BigDecimal creditLimit;
	
	/**
	 * 
	 */
	private String customerEmail;

	/**
	 * 
	 * @return
	 */
	public int getCustomerId() {
		return customerId;
	}

	/**
	 * 
	 * @param customerId
	 */
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	/**
	 * 
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * 
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * 
	 * @return
	 */
	public String getLastAddress1() {
		return lastAddress1;
	}

	/**
	 * 
	 * @param lastAddress1
	 */
	public void setLastAddress1(String lastAddress1) {
		this.lastAddress1 = lastAddress1;
	}

	/**
	 * 
	 * @return
	 */
	public String getLastAddress2() {
		return lastAddress2;
	}

	/**
	 * 
	 * @param lastAddress2
	 */
	public void setLastAddress2(String lastAddress2) {
		this.lastAddress2 = lastAddress2;
	}

	/**
	 * 
	 * @return
	 */
	public String getCity() {
		return city;
	}

	/**
	 * 
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * 
	 * @return
	 */
	public String getState() {
		return state;
	}

	/**
	 * 
	 * @param state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 
	 * @return
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * 
	 * @param postalCode
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * 
	 * @return
	 */
	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	/**
	 * 
	 * @param phoneNumber1
	 */
	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	/**
	 * 
	 * @return
	 */
	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	/**
	 * 
	 * @param phoneNumber2
	 */
	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	/**
	 * 
	 * @param creditLimit
	 */
	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	/**
	 * 
	 * @return
	 */
	public String getCustomerEmail() {
		return customerEmail;
	}

	/**
	 * 
	 * @param customerEmail
	 */
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	
	

	
}
