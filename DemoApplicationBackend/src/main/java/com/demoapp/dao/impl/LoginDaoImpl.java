package com.demoapp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.demoapp.dao.LoginDao;
import com.demoapp.entities.Customer;
import com.demoapp.entities.User;
import com.demoapp.util.HibernateUtil;

/**
 * 
 * @author ElsonRenatoDeFranca
 *
 */
public class LoginDaoImpl implements LoginDao{

	/**
	 * Select a user based on search criteria
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public User selectUserByName(User user) throws Exception {
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("from User where userName = :searchKey ");
		query.setParameter("searchKey", user.getUserName().toUpperCase().trim());
		List<User> resultList = query.list();
		User resultUser = new User();

		for (int i = 0; i < resultList.size(); i++) {
			resultUser = resultList.get(i);
		}
		
		session.close();
		
		return resultUser;
	}

}
