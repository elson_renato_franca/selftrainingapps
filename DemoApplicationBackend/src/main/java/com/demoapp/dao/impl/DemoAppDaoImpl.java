package com.demoapp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.demoapp.dao.DemoAppDao;
import com.demoapp.entities.Customer;
import com.demoapp.entities.Department;
import com.demoapp.entities.Employee;
import com.demoapp.util.HibernateUtil;

/**
 * 
 * 
 *
 */
public class DemoAppDaoImpl implements DemoAppDao {

	/**
	 * Retrieve a customer based on search key
	 * 
	 * @param customer
	 * @return customer
	 * @throws Exception
	 */
	public Customer selectCustomer(Customer customer) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("from Customer where customerId = :searchKey ");
		query.setParameter("searchKey", customer.getCustomerId());
		List<Customer> resultList = query.list();

		Customer resultCustomer = null;

		if (resultList != null && resultList.size() > 0) {
			resultCustomer = new Customer();
			for (int i = 0; i < resultList.size(); i++) {
				resultCustomer = resultList.get(i);
			}
		}

		session.close();

		return resultCustomer;
	}

	/**
	 * Delete a specific customer based on information provided
	 * 
	 * @param customer
	 * @return successful or failure code
	 * @throws Exception
	 */

	public int deleteCustomer(Customer customer) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();

		String hql = "DELETE FROM Customer " + "WHERE customerId = :customerId";
		Query query = session.createQuery(hql);
		query.setParameter("customerId", customer.getCustomerId());
		int rowsAffected = query.executeUpdate();
		System.out.println("Rows affected: " + rowsAffected);

		session.close();

		return rowsAffected;
	}

	/**
	 * Add a new customer to database table based on information provided
	 * 
	 * @param customer
	 * @throws Exception
	 */
	public void addCustomer(Customer customer) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.save(customer);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		} finally {
			session.close();
		}

	}

	/**
	 * Retrieve all customers available
	 * 
	 * @return List<Customer>
	 * @throws Exception
	 */
	public List<Customer> selectAllCustomers() throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("from Customer");
		List<Customer> allCustomerList = query.list();
		session.close();

		return allCustomerList;
	}

	/**
	 * Update a specific customer
	 * 
	 * @param customer
	 * @return successful or failure code
	 * @throws Exception
	 */
	public int updateCustomer(Customer customer) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();

		String hql = "UPDATE Customer set lastName = :lastName " + "WHERE customerId = :customerId";
		Query query = session.createQuery(hql);
		query.setParameter("lastName", customer.getLastName());
		query.setParameter("customerId", customer.getCustomerId());

		int rowsAffected = query.executeUpdate();

		session.close();

		System.out.println("Rows affected: " + rowsAffected);

		return rowsAffected;
	}
	
	
	/**
	 * Update customer last name
	 * @param customer
	 * @return successful or failure code
	 * @throws Exception
	 */
	public int updateCustomerLastName(Customer customer) throws Exception{
		
		Session session = HibernateUtil.getSessionFactory().openSession();

		String hql = "UPDATE Customer set lastName = :lastName " + "WHERE customerId = :customerId";
		Query query = session.createQuery(hql);
		query.setParameter("lastName", customer.getLastName());
		query.setParameter("customerId", customer.getCustomerId());

		int rowsAffected = query.executeUpdate();

		session.close();

		System.out.println("Rows affected: " + rowsAffected);

		return rowsAffected;

	}
	
	/**
	 * Update customer first name
	 * @param customer
	 * @return successful or failure code
	 * @throws Exception
	 */
	public int updateCustomerFirstName(Customer customer) throws Exception{
		
		Session session = HibernateUtil.getSessionFactory().openSession();

		String hql = "UPDATE Customer set firstName = :firstName " + "WHERE customerId = :customerId";
		Query query = session.createQuery(hql);
		query.setParameter("firstName", customer.getFirstName());
		query.setParameter("customerId", customer.getCustomerId());

		int rowsAffected = query.executeUpdate();

		session.close();

		System.out.println("Rows affected: " + rowsAffected);

		return rowsAffected;

	}

	/**
	 * Select all departments available
	 * 
	 * @return List<Department>
	 * @throws Exception
	 */
	public List<Department> selectAllDepartments() throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("from Department");
		List<Department> allDepartmentList = query.list();
		session.close();
		return allDepartmentList;

	}

	/**
	 * Select all employees available
	 * 
	 * @return List<Employee>
	 * @throws Exception
	 */
	public List<Employee> selectAllEmployees() throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("from Employee");
		List<Employee> allEmployeeList = query.list();
		session.close();
		return allEmployeeList;

	}

	/**
	 * 
	 * @param employee
	 * @throws Exception
	 */
	public void addEmployee(Employee employee) throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			session.save(employee);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		} finally {
			session.close();
		}

	}

	
	

}
