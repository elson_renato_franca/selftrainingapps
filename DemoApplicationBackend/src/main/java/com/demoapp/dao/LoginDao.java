package com.demoapp.dao;

import com.demoapp.entities.User;

/**
 * 
 * This class searches for valid users on database 
 *
 */
public interface LoginDao {
	
	/**
	 * Select a user based on search criteria
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public User selectUserByName(User user) throws Exception;

}
