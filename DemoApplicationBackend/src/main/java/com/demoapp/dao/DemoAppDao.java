package com.demoapp.dao;

import java.util.List;

import com.demoapp.entities.Customer;
import com.demoapp.entities.Department;
import com.demoapp.entities.Employee;

/**
 * 
 * This class provides the interface to database access
 *
 */
public interface DemoAppDao {

	/**
	 * Retrieve a customer based on search key
	 * @param customer
	 * @return customer
	 * @throws Exception
	 */
	public Customer selectCustomer(Customer customer) throws Exception;
	
	/**
	 * Delete a specific customer based on information provided
	 * @param customer
	 * @return successful or failure code
	 * @throws Exception
	 */
	public int deleteCustomer(Customer customer)throws Exception;
	
	/**
	 * Add a new customer to database table based on information provided
	 * @param customer
	 * @throws Exception
	 */
	public void addCustomer(Customer customer) throws Exception;
	
	/**
	 * Retrieve all customers available
	 * @return List<Customer>
	 * @throws Exception
	 */
	public List<Customer> selectAllCustomers() throws Exception;
	
	
	/**
	 * Update a specific customer
	 * @param customer
	 * @return successful or failure code
	 * @throws Exception
	 */
	public int updateCustomer(Customer customer) throws Exception;
	

	/**
	 * Update customer last name
	 * @param customer
	 * @return successful or failure code
	 * @throws Exception
	 */
	public int updateCustomerLastName(Customer customer) throws Exception;
	
	/**
	 * Update customer first name
	 * @param customer
	 * @return successful or failure code
	 * @throws Exception
	 */
	public int updateCustomerFirstName(Customer customer) throws Exception;
	
	
	/**
	 * Select all available departments
	 * @return List<Department>
	 * @throws Exception
	 */
	public List<Department> selectAllDepartments()throws Exception;
	
	
	/**
	 * Select all available employees
	 * @return List<Employee>
	 * @throws Exception
	 */
	public List<Employee> selectAllEmployees()throws Exception;
	
	/**
	 * 
	 * @param employee
	 * @throws Exception
	 */
	public void addEmployee(Employee employee)throws Exception;
	
	
}
