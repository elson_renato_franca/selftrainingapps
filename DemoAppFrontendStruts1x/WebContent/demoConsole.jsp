<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Demo Console</title>
</head>
<body>
	<div id="top">
		<p>
			<a href="mainMenu.jsp">back</a> <a href="logout.jsp">Log out</a>
		</p>
	</div>
	
	<div id="content">
		<p>Demo console page</p>
		<h3>Welcome  ${LoginForm.userName}</h3>
	</div>
	
	<div id="footer">
		<p>
			footer must be here
		</p>
	</div>
	
	
		
</body>
</html>