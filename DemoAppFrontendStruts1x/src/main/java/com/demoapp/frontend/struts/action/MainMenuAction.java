package com.demoapp.frontend.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.demoapp.frontend.struts.validator.util.DemoAppEvents;

/**
 * 
 * 
 *
 */
public class MainMenuAction extends Action {

	
	/**
	 * This method redirects the page to admin console 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward redirectToAdminConsole(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
			return mapping.findForward(DemoAppEvents.ADMIN_CONSOLE_EVENT);
		
	}
	
	/**
	 * This method redirects the page to demo console 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward redirectToDemoConsole(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
			return mapping.findForward(DemoAppEvents.DEMO_CONSOLE_EVENT);
		
	}
	
	/**
	 * This method redirects the page to logout
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward logout(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
			return mapping.findForward(DemoAppEvents.LOGOUT_EVENT);
		
	}
	

	/**
	 * This method redirects to about us page
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward redirectToAboutUs(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
			return mapping.findForward(DemoAppEvents.ABOUT_US_EVENT);
		
	}

}
