package com.demoapp.frontend.struts.validator;

import org.apache.struts.validator.ValidatorForm;

/**
 * 
 * 
 *
 */
public class LoginFormValidator extends ValidatorForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	private String userName;
	
	/**
	 * 
	 */
	private String password;

	/**
	 * 
	 * @return
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * 
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
