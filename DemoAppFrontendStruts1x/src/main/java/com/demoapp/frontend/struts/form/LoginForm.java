package com.demoapp.frontend.struts.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.demoapp.frontend.struts.validator.util.LoginFormValidatorUtil;

/**
 * 
 * 
 *
 */
public class LoginForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private String userName;

	/**
	 * 
	 */
	private String password;

	/**
	 * 
	 */
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		try {
			errors.add(validateLogin(userName));
			errors.add(validatePassword(password));

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return errors;
	}

	/**
	 * 
	 * @param login
	 * @return
	 * @throws Exception
	 */
	public ActionErrors validateLogin(String login) throws Exception {

		ActionErrors loginErrors = new ActionErrors();

		if (userName == null || userName.trim().equals("")) {
			loginErrors.add("userName", new ActionMessage("error.username.required"));
		} else {
			if (userName.length() < LoginFormValidatorUtil.MIN_LEN) {
				loginErrors.add("userName", new ActionMessage("error.username.minlength"));
			}
			if(userName.length() > LoginFormValidatorUtil.MAX_LEN){
				loginErrors.add("userName", new ActionMessage("error.username.maxlength"));
			}
			
			

		}

		return loginErrors;
	}

	/**
	 * 
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public ActionErrors validatePassword(String password) throws Exception {

		ActionErrors passwordErrors = new ActionErrors();

		if (password == null || password.isEmpty()) {
			passwordErrors.add("password", new ActionMessage("error.password.required"));
		} else {
			if (password.length() < LoginFormValidatorUtil.MIN_LEN) {
				passwordErrors.add("password", new ActionMessage("error.password.minlength"));
			}
			if(password.length() > LoginFormValidatorUtil.MAX_LEN){
				passwordErrors.add("password", new ActionMessage("error.password.maxlength"));
			}

		}

		return passwordErrors;
	}

	/**
	 * 
	 * @return
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * 
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
