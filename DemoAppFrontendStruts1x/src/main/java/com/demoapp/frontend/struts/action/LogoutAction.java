package com.demoapp.frontend.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.demoapp.frontend.struts.validator.util.DemoAppEvents;

/**
 * 
 * 
 *
 */
public class LogoutAction extends Action{



	/**
	 * This method redirects the flow to acknowledge page 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
			return mapping.findForward(DemoAppEvents.SUCCESS_EVENT);
		
	}
}
