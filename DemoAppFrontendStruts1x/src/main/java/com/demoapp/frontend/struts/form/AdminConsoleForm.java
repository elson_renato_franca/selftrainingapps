package com.demoapp.frontend.struts.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.validator.ValidatorForm;

import com.demoapp.frontend.struts.validator.util.LoginFormValidatorUtil;

/**
 * 
 * 
 *
 */
public class AdminConsoleForm extends ValidatorForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private String username;
	
	/**
	 * 
	 */
	private String password;
	
	/**
	 * 
	 */
	private String email;
	
	/**
	 * 
	 */
	private String telephone;

	
	/**
	 * 
	 */
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		try {
			errors.add(validateLogin(username));
			errors.add(validatePassword(password));
			errors.add(validateEmail(email));
			errors.add(validateTelephone(telephone));
			
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return errors;
	}
	
	/**
	 * 
	 * @param login
	 * @return
	 * @throws Exception
	 */
	public ActionErrors validateLogin(String login) throws Exception {

		ActionErrors loginErrors = new ActionErrors();

		if (username == null || username.trim().equals("")) {
			loginErrors.add("userName", new ActionMessage("error.username.required"));
		} else {
			if (username.length() < LoginFormValidatorUtil.MIN_LEN) {
				loginErrors.add("userName", new ActionMessage("error.username.minlength"));
			}
			if(username.length() > LoginFormValidatorUtil.MAX_LEN){
				loginErrors.add("userName", new ActionMessage("error.username.maxlength"));
			}
			
			

		}

		return loginErrors;
	}

	/**
	 * 
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public ActionErrors validatePassword(String password) throws Exception {

		ActionErrors passwordErrors = new ActionErrors();

		if (password == null || password.isEmpty()) {
			passwordErrors.add("password", new ActionMessage("error.password.required"));
		} else {
			if (password.length() < LoginFormValidatorUtil.MIN_LEN) {
				passwordErrors.add("password", new ActionMessage("error.password.minlength"));
			}
			if(password.length() > LoginFormValidatorUtil.MAX_LEN){
				passwordErrors.add("password", new ActionMessage("error.password.maxlength"));
			}

		}

		return passwordErrors;
	}

	/**
	 * 
	 * @param email
	 * @return
	 * @throws Exception
	 */
	public ActionErrors validateEmail(String email) throws Exception {

		ActionErrors loginErrors = new ActionErrors();

		if (email == null || email.isEmpty()) {
			loginErrors.add("Email", new ActionMessage("error.email.required"));
		} 
		return loginErrors;
	}
	
	/**
	 * 
	 * @param telephone
	 * @return
	 * @throws Exception
	 */
	public ActionErrors validateTelephone(String telephone) throws Exception {

		ActionErrors loginErrors = new ActionErrors();

		if (telephone == null || telephone.trim().equals("")) {
			loginErrors.add("Telephone", new ActionMessage("error.telephone.required"));
		} 
		return loginErrors;
	}


	
	/**
	 * 
	 * @return
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 
	 * @return
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * 
	 * @param telephone
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	
}
