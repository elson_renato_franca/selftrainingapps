<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<html>
<head>
<title>Login Page</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div style="color:red">
    	<html:errors />
    </div>
    <html:form action="/Login" method="post">
        User Name :<html:text name="LoginForm" property="userName" errorStyleClass="error" errorKey="org.apache.struts.action.ERROR"/><br>
        Password  :<html:password name="LoginForm" property="password" errorStyleClass="error" errorKey="org.apache.struts.action.ERROR"/><br>
        <html:submit value="Login" />
    </html:form>
</body>
</html>