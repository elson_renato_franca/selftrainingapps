<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin console page</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="top">
		<p>
			<a href="mainMenu.jsp">back</a> <a href="logout.jsp">Log out</a>
		</p>
	</div>
	
	<div style="color:red">
    	<html:errors />
    </div>

	<div id="content">
		<html:form action="/AdminConsole" method="post">
        User Name :<html:text name="adminConsoleForm"
				property="username" />
			<br>
        Password  :<html:password name="adminConsoleForm"
				property="password" />
			<br>
		Email  :<html:text name="adminConsoleForm" property="email" />
			<br>
		Telephone  :<html:text name="adminConsoleForm"
				property="telephone" />
			<br>
			<html:submit value="Save" />
		</html:form>
	</div>

	<div id="footer">
		<p>footer must be here</p>
	</div>
</html>