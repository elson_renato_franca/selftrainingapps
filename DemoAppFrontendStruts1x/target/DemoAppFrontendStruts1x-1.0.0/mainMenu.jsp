<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome page | Hello World Struts application in Eclipse</title>
</head>
<body>
	<h3>You are logged as ${LoginForm.userName}</h3>
	<div id="top">
		<p>
			<a href="adminConsole.jsp">Admin console</a> 
			<a href="demoConsole.jsp">Demo console</a> 
			<a href="aboutUs.jsp">About us</a>
			<a href="logout.jsp">Log out</a>
		</p>

	</div>
	<div id="content">
		<h1>Footer always in bottom</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla
			aliquet imperdiet justo, quis congue velit egestas quis. Suspendisse
			ac tellus vitae est ultrices imperdiet eget at dolor. Donec sit amet
			viverra arcu. Aenean pulvinar vehicula justo, tincidunt congue nisi
			pharetra eu. Suspendisse potenti. Sed libero quam, lacinia at
			facilisis eu, egestas sodales dolor. Duis gravida, diam interdum
			cursus dictum, ligula libero pellentesque lacus, vitae viverra nulla
			metus id erat. Suspendisse fermentum aliquam hendrerit. Sed vulputate
			massa ut felis sagittis molestie. Suspendisse facilisis condimentum
			diam, a hendrerit urna venenatis sit amet. Cras orci diam, aliquam
			quis fringilla ac, euismod vel orci. Quisque ac pharetra nisi.
			Quisque et orci ligula. Praesent lacus felis, bibendum ac varius ac,
			eleifend in nulla. Aliquam sodales porttitor iaculis. Aliquam pretium
			risus ac neque egestas convallis. Sed dignissim massa ut odio
			vestibulum gravida ut vel nulla.</p>
		<p>Sed non lacus in nibh lobortis imperdiet.</p>
	</div>
	<div id="footer">
		<p></p>
	</div>
</body>
</html>