<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
</head>
<body>
	<f:view>
		<h:form id="loginForm">
			<h:outputText value="Login:" />
			<h:inputText value="#{loginMB.userCredentialsVO.username}" />
			<br></br>
			<h:outputText value="Password:" />
			<h:inputSecret value="#{loginMB.userCredentialsVO.password}"/>
			<br></br>
			<h:commandButton value="Submit" action="#{authResultViewBean.authenticate}"></h:commandButton>
		</h:form>
	</f:view>
</body>
</html>