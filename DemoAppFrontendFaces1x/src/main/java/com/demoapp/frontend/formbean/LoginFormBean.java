package com.demoapp.frontend.formbean;

import java.io.Serializable;

import com.demoapp.backend.model.vo.UserCredentialsVO;



/**
 * 
 * This class holds all information provided by the user
 *
 */
public class LoginFormBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private UserCredentialsVO userCredentialsVO;

	/**
	 * 
	 * @return
	 */
	public UserCredentialsVO getUserCredentialsVO() {
		return userCredentialsVO;
	}

	/**
	 * 
	 * @param userCredentialsVO
	 */
	public void setUserCredentialsVO(UserCredentialsVO userCredentialsVO) {
		this.userCredentialsVO = userCredentialsVO;
	}
	
	
	
	
}
