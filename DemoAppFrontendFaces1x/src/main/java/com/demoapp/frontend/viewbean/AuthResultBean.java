package com.demoapp.frontend.viewbean;

import java.io.Serializable;

import javax.faces.context.FacesContext;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.backend.model.to.UserCredentialsTO;
import com.demoapp.backend.model.vo.UserCredentialsVO;
import com.demoapp.backend.service.LoginHandlerService;
import com.demoapp.frontend.util.LoginPageOutcome;



/**
 * 
 * 
 *
 */
public class AuthResultBean implements Serializable{

	/**
	 * 
	 */
	private LoginHandlerService loginService;
	
	/**
	 * 
	 */
	private ApplicationContext applicationContext;
	
	/**
	 * 
	 */
	private UserCredentialsVO userCredentialsVO;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	public AuthResultBean(){
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		this.applicationContext = new ClassPathXmlApplicationContext(new String[] { "SpringBeans.xml" });
		this.loginService = (LoginHandlerService)applicationContext.getBean("loginService");
		//this.userCredentialsVO = (UserCredentialsVO)facesContext.getApplication().evaluateExpressionGet(facesContext, "#{userCredentialsBean}", UserCredentialsVO.class);
		
	}
	
	/**
	 * 
	 * @return
	 */
	public String authenticate(){
		
		String authenticationResult=LoginPageOutcome.FAILURE;
		
		try {
			UserCredentialsTO userCredentialsTO = this.loginService.findByLogin(userCredentialsVO);
			
			if(userCredentialsTO != null){
				if(userCredentialsTO.getUsername() !=null){
					authenticationResult=LoginPageOutcome.SUCCESS;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return authenticationResult;
	}


	/**
	 * 
	 * @return
	 */
	public LoginHandlerService getLoginService() {
		return loginService;
	}

	/**
	 * 
	 * @param loginService
	 */
	public void setLoginService(LoginHandlerService loginService) {
		this.loginService = loginService;
	}

	/**
	 * 
	 * @return
	 */
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * 
	 * @param applicationContext
	 */
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	/**
	 * 
	 * @return
	 */
	public UserCredentialsVO getUserCredentialsVO() {
		return userCredentialsVO;
	}

	/**
	 * 
	 * @param userCredentialsVO
	 */
	public void setUserCredentialsVO(UserCredentialsVO userCredentialsVO) {
		this.userCredentialsVO = userCredentialsVO;
	}
	
}
