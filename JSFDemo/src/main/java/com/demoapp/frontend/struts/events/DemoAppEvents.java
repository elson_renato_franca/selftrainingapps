package com.demoapp.frontend.struts.events;

/**
 * 
 * 
 *
 */
public interface DemoAppEvents {

	/**
	 * 
	 */
	public static final String ADMIN_CONSOLE_EVENT="adminconsole";
	
	/**
	 * 
	 */
	public static final String DEMO_CONSOLE_EVENT="democonsole";
	
	/**
	 * 
	 */
	public static final String ABOUT_US_EVENT="aboutUs";
	

	/**
	 * 
	 */
	public static final String LOGOUT_EVENT="logout";
	
	/**
	 * 
	 */
	public static final String BACK_EVENT="back";
	
	/**
	 * 
	 */
	public static final String SUCCESS_EVENT="success";
	
	/**
	 * 
	 */
	public static final String FAILURE_EVENT="failure";
	
	
}
