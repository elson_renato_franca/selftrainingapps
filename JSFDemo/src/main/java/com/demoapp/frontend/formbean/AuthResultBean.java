package com.demoapp.frontend.formbean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.backend.model.vo.UserCredentialsVO;
import com.demoapp.backend.service.LoginHandlerService;
import com.demoapp.frontend.struts.events.DemoAppEvents;


@ManagedBean(name="authResultBean")
@RequestScoped
public class AuthResultBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Invokes the backend application to check database
	 */
	private LoginHandlerService loginService;
	/**
	 * 
	 */
	private LoginFormBean loginFormBean;
	
	/**
	 * 
	 */
	private UserCredentialsVO userCredentialsVO;
	
	/**
	 * 
	 * @return
	 */
	public String authenticate(){
		
		String outcome=DemoAppEvents.FAILURE_EVENT;
		
		try {
			ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "SpringBeans.xml" });
			this.loginService = (LoginHandlerService) context.getBean("loginService");
			userCredentialsVO =  (UserCredentialsVO)  context.getBean("userCredentialsVO");
			userCredentialsVO.setUsername(loginFormBean.getUsername());
			userCredentialsVO.setPassword(loginFormBean.getPassword());
			this.loginService.findByLogin(userCredentialsVO);
			outcome=DemoAppEvents.SUCCESS_EVENT;
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Inside result bean");
		return outcome;
	}

	/**
	 * @return the loginService
	 */
	public LoginHandlerService getLoginService() {
		return loginService;
	}

	/**
	 * @param loginService the loginService to set
	 */
	public void setLoginService(LoginHandlerService loginService) {
		this.loginService = loginService;
	}

	/**
	 * @return the loginFormBean
	 */
	public LoginFormBean getLoginFormBean() {
		return loginFormBean;
	}

	/**
	 * @param loginFormBean the loginFormBean to set
	 */
	public void setLoginFormBean(LoginFormBean loginFormBean) {
		this.loginFormBean = loginFormBean;
	}

	/**
	 * @return the userCredentialsVO
	 */
	public UserCredentialsVO getUserCredentialsVO() {
		return userCredentialsVO;
	}

	/**
	 * @param userCredentialsVO the userCredentialsVO to set
	 */
	public void setUserCredentialsVO(UserCredentialsVO userCredentialsVO) {
		this.userCredentialsVO = userCredentialsVO;
	}

	
	
	
	
}
