package com.demoapp.services.impl;

import java.util.List;

import com.demoapp.dao.CustomerHandlerDAO;
import com.demoapp.services.CustomerHandlerService;
import com.demoapp.util.to.CustomerDetailsTO;
import com.demoapp.util.vo.CustomerDetailsVO;

/**
 * 
 * 
 *
 */
public class CustomerHandlerServiceImpl implements CustomerHandlerService {

	/**
	 * 
	 */
	private CustomerHandlerDAO customerHandlerDAO;

	/**
	 * Add a new customer to database
	 * 
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public int addCustomer(CustomerDetailsVO customerDetailsVO) throws Exception {
		int affectedRows = this.customerHandlerDAO.addCustomer(customerDetailsVO);
		return affectedRows;
	}

	/**
	 * Delete an existing customer
	 * 
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public void deleteCustomer(CustomerDetailsVO customerDetailsVO) throws Exception {
		this.customerHandlerDAO.deleteCustomer(customerDetailsVO);

	}

	/**
	 * Search customer by Id
	 * 
	 * @param customerDetailsVO
	 * @return CustomerDetailsTO
	 * @throws Exception
	 */
	public CustomerDetailsTO searchCustomerById(CustomerDetailsVO customerDetailsVO) throws Exception {
		return customerHandlerDAO.searchCustomerById(customerDetailsVO);
	}

	/**
	 * Update a customer
	 * 
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public void updateCustomer(CustomerDetailsVO customerDetailsVO) throws Exception {
		this.customerHandlerDAO.updateCustomer(customerDetailsVO);
	}

	/**
	 * Search for all customers
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<CustomerDetailsTO> searchAllCustomers() throws Exception {
		List<CustomerDetailsTO> resultList = this.customerHandlerDAO.searchAllCustomers();
		return resultList;

	}

	/**
	 * 
	 * @return
	 */
	public CustomerHandlerDAO getCustomerHandlerDAO() {
		return customerHandlerDAO;
	}

	/**
	 * 
	 * @param customerHandlerDAO
	 */
	public void setCustomerHandlerDAO(CustomerHandlerDAO customerHandlerDAO) {
		this.customerHandlerDAO = customerHandlerDAO;
	}

}
