package com.demoapp.services;

import com.demoapp.util.to.CustomerDetailsTO;
import com.demoapp.util.vo.CustomerDetailsVO;

/**
 * 
 * 
 *
 */
public interface CustomerHandlerService {

	/**
	 *  Add a new customer to database
	 * @param customerDetailsVO
	 * @return
	 * @throws Exception
	 */
	public int addCustomer(CustomerDetailsVO customerDetailsVO) throws Exception;
	
	/**
	 * Delete an existing customer
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public void deleteCustomer(CustomerDetailsVO customerDetailsVO) throws Exception;
	
	
	/**
	 * Search customer by Id
	 * @param customerDetailsVO
	 * @return CustomerDetailsTO
	 * @throws Exception
	 */
	public CustomerDetailsTO searchCustomerById(CustomerDetailsVO customerDetailsVO) throws Exception;
	
}
