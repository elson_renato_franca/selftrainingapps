package com.demoapp.restful.resources;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.backend.mongodb.model.to.ContactTO;
import com.demoapp.backend.mongodb.model.vo.ContactVO;
import com.demoapp.backend.mongodb.service.ContactHandlerService;


/**
 * URL to call webservices:
 * 
 * http://localhost:8080/DemoAppWebServiceREST/rest/contacthandler/findByName
 * http://localhost:8080/DemoAppWebServiceREST/rest/contacthandler/findById
 * http://localhost:8080/DemoAppWebServiceREST/rest/contacthandler/findAll
 *
 */
@Path("/contacthandler")
public class ContactHandler {

	/**
	 * Exposes all services in a json format
	 */
	private ContactHandlerService contactHandlerService;

	/**
	 * 
	 */
	private ApplicationContext context;

	/**
	 * 
	 */
	private ContactVO contactVO;

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(ContactHandler.class);

	
	/**
	 * 
	 */
	public ContactHandler() {
		this.context = new ClassPathXmlApplicationContext("SpringBeans.xml");
		this.contactHandlerService = (ContactHandlerService) context.getBean("contactHandlerService");
		this.contactVO = (ContactVO) context.getBean("contactVO");

	}

	/**
	 * 
	 * @return
	 */
	@GET
	@Path("/findByName")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String findContactByName() {

		ContactTO resultQuery = new ContactTO();
		try {
			resultQuery = this.contactHandlerService.findContactByName(contactVO);
			log.debug("result query: " + resultQuery);
		} catch (Exception e) {
			log.error("Error during service invocation: " + e);
			e.printStackTrace();
		}
		return resultQuery.toString();
	}

	@GET
	@Path("/findById")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String findContactById() {
		ContactTO resultQuery = new ContactTO();
		try {
			resultQuery = this.contactHandlerService.findContactById(contactVO);
			log.debug("result query: " + resultQuery);
		} catch (Exception e) {
			log.error("Error during service invocation: " + e);
			e.printStackTrace();
		}
		return resultQuery.toString();
	}

	@GET
	@Path("/findAll")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<ContactTO> findAll() {
		List<ContactTO> resultList = null;
		try {
			resultList = this.contactHandlerService.findAll();
		} catch (Exception e) {
			log.error("Error during service invocation: " + e);
			e.printStackTrace();
		}
		return resultList;
	}

	@POST
	@Path("/saveContact")
	@Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response saveContact(ContactVO contactVO) throws Exception {

		int result=0;
		
		try {
			if(contactVO !=null){
				log.debug(contactVO.toString());
				result = this.contactHandlerService.saveContact(contactVO);
			}
			log.debug("REST webservices POST HTTP method - Object saved successfully"+result);
		} catch (IOException e) {
			e.printStackTrace();
		}catch(URISyntaxException uriEx){
			uriEx.printStackTrace();
		}
		return Response.status(201).entity(result).build();
	}
	
	

	/**
	 * 
	 * @return
	 */
	public ContactHandlerService getContactHandlerService() {
		return contactHandlerService;
	}

	/**
	 * 
	 * @param contactHandlerService
	 */
	public void setContactHandlerService(ContactHandlerService contactHandlerService) {
		this.contactHandlerService = contactHandlerService;
	}

	/**
	 * 
	 * @return
	 */
	public ApplicationContext getContext() {
		return context;
	}

	/**
	 * 
	 * @param context
	 */
	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	/**
	 * 
	 * @return
	 */
	public ContactVO getContactVO() {
		return contactVO;
	}

	/**
	 * 
	 * @param contactVO
	 */
	public void setContactVO(ContactVO contactVO) {
		this.contactVO = contactVO;
	}

}
