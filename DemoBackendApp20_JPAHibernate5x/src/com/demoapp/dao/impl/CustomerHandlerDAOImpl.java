package com.demoapp.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.demoapp.dao.CustomerHandlerDAO;
import com.demoapp.entities.Customer;
import com.demoapp.util.to.CustomerDetailsTO;
import com.demoapp.util.vo.CustomerDetailsVO;

/**
 * 
 * 
 *
 */
public class CustomerHandlerDAOImpl implements CustomerHandlerDAO {

	/**
	 * 
	 */
	private EntityManager entityManager;

	/**
	 * 
	 */
	EntityManagerFactory entityManagerFactory;
	
	/**
	 * Add a new customer to database
	 * 
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public CustomerDetailsTO addCustomer(CustomerDetailsVO customerDetailsVO) throws Exception {
		
		CustomerDetailsTO customerDetailsTO = null;
		/*
		 try {
		      entityManager.getTransaction().begin();
		      System.out.println("persisting customer..");
		      if(customerDetailsTO.getCustomerId() == null) {
		        entityManager.persist(customerDetailsVO);
		      } else {
		    	  customerDetailsTO = entityManager.merge(customerDetailsTO);
		      }
		      entityManager.getTransaction().commit();
		    } finally {
		      entityManager.close();
		    }*/
		    return customerDetailsTO;
	}

	/**
	 * Delete an existing customer
	 * 
	 * @param customerDetailsVO
	 * @throws Exception
	 */
	public void deleteCustomer(CustomerDetailsVO customerDetailsVO) throws Exception {
		// TODO Auto-generated method stub

	}

	/**
	 * Search customer by Id
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public CustomerDetailsTO searchCustomerById(Long id) throws Exception {
		
		CustomerDetailsTO customerDetailsTO = null;
		this.entityManagerFactory = Persistence.createEntityManagerFactory("myPersistentUnit");
		this.entityManager = this.entityManagerFactory.createEntityManager();
		
	    try {
	    	Customer customer = entityManager.find(Customer.class, id);
	    	customerDetailsTO = new CustomerDetailsTO();
        	customerDetailsTO.setCity(customer.getCity());
	    	customerDetailsTO.setFirstName(customer.getFirstName());
	    	customerDetailsTO.setLastName(customer.getLastName());
	    	
	    } finally {
	      entityManager.close();
	    }
	    
		return customerDetailsTO;
	}
	
	/**
	 * 
	 * @return
	 */
	
	public List<CustomerDetailsTO> getAllDepartments() {
	    List<CustomerDetailsTO> depts = this.entityManager.createQuery("Select a From DepartmentEntity a",
	    		CustomerDetailsTO.class).getResultList();
	    return depts;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	
	
}
