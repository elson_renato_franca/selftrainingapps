package com.demoapp.backend.util;

/**
 * 
 * 
 *
 */
public interface CustomerHandlerConstants {

	/**
	 * 
	 */
	public static final String LAST_NAME="lastName";
	
	/**
	 * 
	 */
	public static final String FIRST_NAME="firstName";
	
	/**
	 * 
	 */
	public static final String CITY="city";
	
	/**
	 * 
	 */
	public static final String STATE="state";
	
}
