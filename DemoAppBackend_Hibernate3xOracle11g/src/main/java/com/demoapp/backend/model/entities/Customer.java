
package com.demoapp.backend.model.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DEMO_CUSTOMERS")
public class Customer {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CUSTOMER_ID")
	private int customerId;
	
	@Column(name="CUST_FIRST_NAME")
	private String firstName;
	
	@Column(name="CUST_LAST_NAME")
	private String lastName;
	
	@Column(name="CUST_STREET_ADDRESS1")
	private String customerStreetAddress1;
	
	@Column(name="CUST_STREET_ADDRESS2")
	private String customerStreetAddress2;
	
	@Column(name="CUST_CITY")
	private String city;
	
	@Column(name="CUST_STATE")
	private String state;
	
	@Column(name="CUST_POSTAL_CODE")
	private String postalCode;
	
	@Column(name="PHONE_NUMBER1")
	private String phoneNumber1;
	
	@Column(name="PHONE_NUMBER2")
	private String phoneNumber2;
	
	@Column(name="CREDIT_LIMIT")
	private BigDecimal creditLimit;
	
	@Column(name="CUST_EMAIL")
	private String customerEmail;

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCustomerStreetAddress1() {
		return customerStreetAddress1;
	}

	public void setCustomerStreetAddress1(String customerStreetAddress1) {
		this.customerStreetAddress1 = customerStreetAddress1;
	}

	public String getCustomerStreetAddress2() {
		return customerStreetAddress2;
	}

	public void setCustomerStreetAddress2(String customerStreetAddress2) {
		this.customerStreetAddress2 = customerStreetAddress2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	
	
}
