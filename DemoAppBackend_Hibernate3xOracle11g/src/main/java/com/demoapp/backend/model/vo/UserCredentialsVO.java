package com.demoapp.backend.model.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 *
 */
public class UserCredentialsVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	

	/**
	 * 
	 */
	private int userId;
	/**
	 * 
	 */
	private String username;
	
	/**
	 * 
	 */
	private String password;
	/**
	 * 
	 */
	private Date expirationDate;
	
	/**
	 * 
	 */
	private Date creationDate;
	
	/**
	 * 
	 */
	private Integer quota;
	
	/**
	 * 
	 */
	private String products;
	
	/**
	 * 
	 */
	private String adminUserFlag;

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the expirationDate
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	/**
	 * @return the quota
	 */
	public Integer getQuota() {
		return quota;
	}

	/**
	 * @param quota the quota to set
	 */
	public void setQuota(Integer quota) {
		this.quota = quota;
	}

	/**
	 * @return the products
	 */
	public String getProducts() {
		return products;
	}

	/**
	 * @param products the products to set
	 */
	public void setProducts(String products) {
		this.products = products;
	}

	/**
	 * @return the adminUserFlag
	 */
	public String getAdminUserFlag() {
		return adminUserFlag;
	}

	/**
	 * @param adminUserFlag the adminUserFlag to set
	 */
	public void setAdminUserFlag(String adminUserFlag) {
		this.adminUserFlag = adminUserFlag;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adminUserFlag == null) ? 0 : adminUserFlag.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((expirationDate == null) ? 0 : expirationDate.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((products == null) ? 0 : products.hashCode());
		result = prime * result + ((quota == null) ? 0 : quota.hashCode());
		result = prime * result + userId;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserCredentialsVO other = (UserCredentialsVO) obj;
		if (adminUserFlag == null) {
			if (other.adminUserFlag != null)
				return false;
		} else if (!adminUserFlag.equals(other.adminUserFlag))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (expirationDate == null) {
			if (other.expirationDate != null)
				return false;
		} else if (!expirationDate.equals(other.expirationDate))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (products == null) {
			if (other.products != null)
				return false;
		} else if (!products.equals(other.products))
			return false;
		if (quota == null) {
			if (other.quota != null)
				return false;
		} else if (!quota.equals(other.quota))
			return false;
		if (userId != other.userId)
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserCredentialsVO [userId=" + userId + ", username=" + username + ", password=" + password
				+ ", expirationDate=" + expirationDate + ", creationDate=" + creationDate + ", quota=" + quota
				+ ", products=" + products + ", adminUserFlag=" + adminUserFlag + "]";
	}
	

}
