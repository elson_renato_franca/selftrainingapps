package com.demoapp.backend.converter;

import com.demoapp.backend.model.entities.User;
import com.demoapp.backend.model.to.UserCredentialsTO;
import com.demoapp.backend.model.vo.UserCredentialsVO;


/**
 * 
 * 
 *
 */
public class LoginHandlerConverter {

	/**
	 * 
	 * @param User
	 * @return UserCredentialsTO
	 * @throws Exception
	 */
	public UserCredentialsTO  fromEntityToTransitionalObject(User user)throws Exception{
		
		UserCredentialsTO userCredentialsTO = new UserCredentialsTO();
		
		userCredentialsTO.setAdminUserFlag(user.getAdminUserFlag());
		userCredentialsTO.setCreationDate(user.getCreationDate());
		userCredentialsTO.setExpirationDate(user.getExpirationDate());
		userCredentialsTO.setPassword(user.getPassword());
		userCredentialsTO.setProducts(user.getProducts());
		userCredentialsTO.setQuota(user.getQuota());
		userCredentialsTO.setUserId(user.getUserId());
		userCredentialsTO.setUsername(user.getUsername());
		return userCredentialsTO;
	}
	
	/**
	 * 
	 * @param UserCredentialsVO
	 * @return DBUser
	 * @throws Exception
	 */
	public User fromVOtoDatabaseEntity(UserCredentialsVO userCredentialsVO)throws Exception{
		User user = new User();
		user.setAdminUserFlag(userCredentialsVO.getAdminUserFlag());
		user.setCreationDate(userCredentialsVO.getCreationDate());
		user.setExpirationDate(userCredentialsVO.getExpirationDate());
		user.setPassword(userCredentialsVO.getPassword());
		user.setProducts(userCredentialsVO.getProducts());
		user.setQuota(userCredentialsVO.getQuota());
		user.setUserId(userCredentialsVO.getUserId());
		user.setUsername(userCredentialsVO.getUsername());
		
		return user;
	}
	
	

	/**
	 * 
	 * @param contactTO
	 * @return
	 * @throws Exception
	 */
	public User fromTOtoDatabaseEntity(UserCredentialsTO userCredentialsTO) throws Exception {

		User user = new User();

		user.setAdminUserFlag(userCredentialsTO.getAdminUserFlag());
		user.setCreationDate(userCredentialsTO.getCreationDate());
		user.setExpirationDate(userCredentialsTO.getExpirationDate());
		user.setPassword(userCredentialsTO.getPassword());
		user.setProducts(userCredentialsTO.getProducts());
		user.setQuota(userCredentialsTO.getQuota());
		user.setUserId(userCredentialsTO.getUserId());
		user.setUsername(userCredentialsTO.getUsername());
		
		return user;
	}

}
