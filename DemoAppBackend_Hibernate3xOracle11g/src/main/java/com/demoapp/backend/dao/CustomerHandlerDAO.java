package com.demoapp.backend.dao;

import java.util.List;

import com.demoapp.backend.model.entities.Customer;


/**
 * 
 * 
 *
 */
public interface CustomerHandlerDAO {


	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Customer> selectAllCustomers()throws Exception;
	
	/**
	 * Delete customer
	 * @param customer
	 * @return rows affected
	 * @throws Exception
	 */
	public int deleteCustomer(Customer customer)throws Exception;
	
	/**
	 * Update Customer first name
	 * @param customer
	 * @return rows affected
	 * @throws Exception
	 */
	public int updateCustomerFirstName(Customer customer)throws Exception;
	
	/**
	 * Update Customer last name
	 * @param customer
	 * @return rows affected
	 * @throws Exception
	 */
	public int updateCustomerLastName(Customer customer)throws Exception;
	
	
	/**
	 * Update Customer state
	 * @param customer
	 * @return rows affected
	 * @throws Exception
	 */
	public int updateCustomerState(Customer customer)throws Exception;
	

	/**
	 * Update Customer city
	 * @param customer
	 * @return rows affected
	 * @throws Exception
	 */
	public int updateCustomerCity(Customer customer)throws Exception;
	
	/**
	 * 
	 * @param customer
	 * @throws Exception
	 */
	public void addCustomer(Customer customer)throws Exception;
}
