package com.demoapp.backend.dao;

import java.util.List;

import com.demoapp.backend.model.entities.User;

/**
 * 
 * 
 *
 */
public interface LoginHandlerDAO {

	/**
	 * 
	 * @param User user
	 * @return user
	 * @throws Exception
	 */
	public User findByLogin(User user) throws Exception;
	
	
	/**
	 * 
	 * @param User user
	 * @return affectedRows
	 * @throws Exception
	 */
	public void saveUser(User user) throws Exception;
	
	/**
	 * 
	 * @param User
	 * @return affectedRows
	 * @throws Exception
	 */
	public int updateUser(User user) throws Exception;
	
	/**
	 * 
	 * @param User
	 * @return affectedRows
	 * @throws Exception
	 */
	public int deleteUser(User user) throws Exception;
	
	/**
	 * 
	 * @param CredentialsVO
	 * @return
	 * @throws Exception
	 */
	public List<User> findAll(User user) throws Exception;
	
	
}
