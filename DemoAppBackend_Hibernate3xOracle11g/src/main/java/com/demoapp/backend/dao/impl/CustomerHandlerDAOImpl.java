package com.demoapp.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.demoapp.backend.dao.CustomerHandlerDAO;
import com.demoapp.backend.model.entities.Customer;
import com.demoapp.backend.util.HibernateUtil;


/**
 * 
 * 
 *
 */
public class CustomerHandlerDAOImpl implements CustomerHandlerDAO {

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Customer> selectAllCustomers() throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		String hql = "FROM Customer";
		Query query = session.createQuery(hql);
		List<Customer> resultList = query.list();

		return resultList;
	}

	/**
	 * Delete customer by its Id
	 * 
	 * @param customer
	 * @return affected rows
	 * @throws Exception
	 */
	public int deleteCustomer(Customer customer) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("delete Customer where customerId = :customerId");
		query.setParameter("customerId", customer.getCustomerId());
		int affectedRows = query.executeUpdate();
		return affectedRows;
	}

	/**
	 * Update Customer details
	 * 
	 * @param customer
	 * @return affected rows
	 * @throws Exception
	 */
	public int updateCustomer(Customer customer) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session
				.createQuery("update Customer set lastName = :lastName" + " where customerId = :customerId");
		query.setParameter("lastName", customer.getLastName());
		query.setParameter("customerId", customer.getCustomerId());
		int result = query.executeUpdate();

		return 0;
	}

	/**
	 * Save a new customer to database
	 * @param customer
	 * @throws Exception
	 */
	public void addCustomer(Customer customer) throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(customer);
		session.getTransaction().commit();
		HibernateUtil.shutdown();
	}

	/**
	 * Update Customer first name
	 * 
	 * @param customer
	 * @return affected rows
	 * @throws Exception
	 */
	public int updateCustomerFirstName(Customer customer) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session
				.createQuery("update Customer set firstName = :firstName" + " where customerId = :customerId");
		query.setParameter("firstName", customer.getFirstName());
		query.setParameter("customerId", customer.getCustomerId());
		int affectedRows = query.executeUpdate();

		return affectedRows;
	}

	/**
	 * Update Customer last name
	 * 
	 * @param customer
	 * @return rows affected
	 * @throws Exception
	 */
	public int updateCustomerLastName(Customer customer) throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session
				.createQuery("update Customer set lastName = :lastName" + " where customerId = :customerId");
		query.setParameter("lastName", customer.getLastName());
		query.setParameter("customerId", customer.getCustomerId());
		int affectedRows = query.executeUpdate();

		return affectedRows;
	}

	/**
	 * Update Customer state
	 * 
	 * @param customer
	 * @return rows affected
	 * @throws Exception
	 */
	public int updateCustomerState(Customer customer) throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("update Customer set state = :state" + " where customerId = :customerId");
		query.setParameter("state", customer.getState());
		query.setParameter("customerId", customer.getCustomerId());
		int affectedRows = query.executeUpdate();

		return affectedRows;

	}

	/**
	 * Update Customer city
	 * 
	 * @param customer
	 * @return rows affected
	 * @throws Exception
	 */
	public int updateCustomerCity(Customer customer) throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("update Customer set city = :city" + " where customerId = :customerId");
		query.setParameter("city", customer.getCity());
		query.setParameter("customerId", customer.getCustomerId());
		int affectedRows = query.executeUpdate();

		return affectedRows;
	}

}
