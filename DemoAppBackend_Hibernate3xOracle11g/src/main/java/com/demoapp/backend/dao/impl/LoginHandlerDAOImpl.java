package com.demoapp.backend.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.demoapp.backend.dao.LoginHandlerDAO;
import com.demoapp.backend.model.entities.Customer;
import com.demoapp.backend.model.entities.User;
import com.demoapp.backend.util.HibernateUtil;

/**
 * 
 * 
 *
 */
public class LoginHandlerDAOImpl implements LoginHandlerDAO{

	@Override
	public User findByLogin(User user) throws Exception {
		User userEntity = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("from User where username = :username ");
		query.setParameter("username", user.getUsername());
		List list = query.list();
		
		if(list !=null){
			for(Object object: list){
			 userEntity = (User)object;	
			}
		}
		return userEntity;
	}

	@Override
	public void saveUser(User user) throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(user);
		session.getTransaction().commit();
		HibernateUtil.shutdown();
	}

	@Override
	public int updateUser(User user) throws Exception {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session
				.createQuery("update User set username = :username" + " where userId = :userId");
		query.setParameter("username", user.getUsername());
		query.setParameter("userId", user.getUserId());
		int result = query.executeUpdate();
		
		return result;

	}

	@Override
	public int deleteUser(User user) throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("delete User where userId = :userId");
		query.setParameter("userId", user.getUserId());
		int affectedRows = query.executeUpdate();
		return affectedRows;

	}

	@Override
	public List<User> findAll(User user) throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		String hql = "FROM User";
		Query query = session.createQuery(hql);
		List<User> resultList = query.list();

		return resultList;
	}

	
}
