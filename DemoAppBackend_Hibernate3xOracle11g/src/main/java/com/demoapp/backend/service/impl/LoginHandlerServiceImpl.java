package com.demoapp.backend.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.QueryTimeoutException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.backend.converter.LoginHandlerConverter;
import com.demoapp.backend.dao.LoginHandlerDAO;
import com.demoapp.backend.model.entities.User;
import com.demoapp.backend.model.to.UserCredentialsTO;
import com.demoapp.backend.model.vo.UserCredentialsVO;
import com.demoapp.backend.mongodb.exception.DemoAppQueryException;
import com.demoapp.backend.service.LoginHandlerService;

/**
 * 
 * 
 *
 */
public class LoginHandlerServiceImpl implements LoginHandlerService{

	
	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(LoginHandlerServiceImpl.class);

	/**
	 * 
	 */
	private LoginHandlerDAO loginHandlerDao;
	
	/**
	 * 
	 */
	private LoginHandlerConverter loginHandlerConverter;
	
	@Override
	public UserCredentialsTO findByLogin(UserCredentialsVO userCredentialsVO) throws Exception {
		

		UserCredentialsTO userCredentialsTO = null;
		
		try {
			User entityUser = this.loginHandlerConverter.fromVOtoDatabaseEntity(userCredentialsVO);
			User resultQuery = this.loginHandlerDao.findByLogin(entityUser);
			if (resultQuery != null) {
				userCredentialsTO = this.loginHandlerConverter.fromEntityToTransitionalObject(resultQuery);
			}

		} catch (NoResultException nre) {
			userCredentialsTO = null;
			log.debug(DemoAppQueryException.NO_RESULT_EXCEPTION + nre);
		} catch (EntityNotFoundException entityNotFoundException) {
			userCredentialsTO = null;
			log.debug(DemoAppQueryException.ENTITY_NOT_FOUND_EXCEPTION + entityNotFoundException);
		}catch(NonUniqueResultException nonUniqueResultException){
			User user = this.loginHandlerConverter.fromVOtoDatabaseEntity(userCredentialsVO);
			userCredentialsTO = this.loginHandlerConverter.fromEntityToTransitionalObject(user);
			log.debug(DemoAppQueryException.NON_UNIQUE_RESULT_EXCEPTION + nonUniqueResultException);
		}
		
		return userCredentialsTO;

	}

	@Override
	public int saveUser(UserCredentialsVO userCredentialsVO) throws Exception {
		int affectedRows = 0;

		try {
			UserCredentialsTO existingContactTO = this.findByLogin(userCredentialsVO);
			
			if (existingContactTO == null) {
				User user = this.loginHandlerConverter.fromVOtoDatabaseEntity(userCredentialsVO);
				this.loginHandlerDao.saveUser(user);
				affectedRows++;
			}

		}  catch (SQLException ex) {
			log.error(DemoAppQueryException.SQL_EXCEPTION + ex);
		} catch (ConstraintViolationException constraintViolationException) {
			log.error(DemoAppQueryException.CONSTRAINT_VIOLATION_EXCEPTION + constraintViolationException);
		} catch (EntityExistsException entityExistsException) {
			log.error(DemoAppQueryException.ENTITY_EXISTS_EXCEPTION + entityExistsException);
		}

		return affectedRows;

	}

	@Override
	public int updateUser(UserCredentialsVO userCredentialsVO) throws Exception {
		int affectedRows = 0;

		try {
			UserCredentialsTO existingContactTO = this.findByLogin(userCredentialsVO);
			
			if (existingContactTO != null) {
				User user = this.loginHandlerConverter.fromVOtoDatabaseEntity(userCredentialsVO);
				this.loginHandlerDao.updateUser(user);
				affectedRows++;
			}

		} catch (IllegalArgumentException e) {
			log.debug(DemoAppQueryException.ILLEGAL_ARGUMENT_EXCEPTION + e);
		} catch (SQLException ex) {
			log.debug(DemoAppQueryException.SQL_EXCEPTION + ex);
		}
		return affectedRows;

	}

	@Override
	public int deleteUser(UserCredentialsVO userCredentialsVO) throws Exception {

		int affectedRows = 0;

		try {
			UserCredentialsTO existingContactTO = this.findByLogin(userCredentialsVO);
			
			if (existingContactTO != null) {
				User user = this.loginHandlerConverter.fromVOtoDatabaseEntity(userCredentialsVO);
				this.loginHandlerDao.deleteUser(user);
				affectedRows++;
			}

		} catch (IllegalArgumentException e) {
			log.debug(DemoAppQueryException.ILLEGAL_ARGUMENT_EXCEPTION + e);
		} catch (SQLException ex) {
			log.debug(DemoAppQueryException.SQL_EXCEPTION + ex);
		}


		return affectedRows;

	}

	@Override
	public List<UserCredentialsTO> findAll(UserCredentialsVO userCredentialsVO) throws Exception {
		
		List<UserCredentialsTO> userCredentialsTOList = null;

		try {
			User entityUser = this.loginHandlerConverter.fromVOtoDatabaseEntity(userCredentialsVO);
			List<User> userListFromDb = this.loginHandlerDao.findAll(entityUser);

			if (userListFromDb != null && !userListFromDb.isEmpty()) {

				userCredentialsTOList = new ArrayList<UserCredentialsTO>();

				for (User userEntity : userListFromDb) {
					UserCredentialsTO userCredentialsTO = this.loginHandlerConverter.fromEntityToTransitionalObject(userEntity);
					userCredentialsTOList.add(userCredentialsTO);
				}
			}

		} catch (QueryTimeoutException queryTimeoutException) {
			userCredentialsTOList = null;
			log.debug(DemoAppQueryException.QUERY_TIMEOUT_EXCEPTION + queryTimeoutException);
		} catch (IllegalStateException illegalStateException) {
			userCredentialsTOList = null;
			log.debug(DemoAppQueryException.ILLEGAL_STATE_EXCEPTION + illegalStateException);
		}

		return userCredentialsTOList;

	}

	
	/**
	 * @return the loginHandlerDao
	 */
	public LoginHandlerDAO getLoginHandlerDao() {
		return loginHandlerDao;
	}

	/**
	 * @param loginHandlerDao the loginHandlerDao to set
	 */
	public void setLoginHandlerDao(LoginHandlerDAO loginHandlerDao) {
		this.loginHandlerDao = loginHandlerDao;
	}

	/**
	 * @return the loginHandlerConverter
	 */
	public LoginHandlerConverter getLoginHandlerConverter() {
		ApplicationContext context = new ClassPathXmlApplicationContext("SpringBeans.xml");
		this.loginHandlerConverter = (LoginHandlerConverter) context.getBean("loginHandlerConverter");
		return loginHandlerConverter;
	}

	/**
	 * @param loginHandlerConverter the loginHandlerConverter to set
	 */
	public void setLoginHandlerConverter(LoginHandlerConverter loginHandlerConverter) {
		this.loginHandlerConverter = loginHandlerConverter;
	}

	

}
