package com.demoapp.backend.service.impl;

import java.util.List;

import com.demoapp.backend.dao.CustomerHandlerDAO;
import com.demoapp.backend.model.entities.Customer;
import com.demoapp.backend.service.CustomerHandlerService;
import com.demoapp.backend.util.CustomerHandlerConstants;


/**
 * 
 * @author userpc
 *
 */
public class CustomerHandlerServiceImpl implements CustomerHandlerService {

	/**
	 * 
	 */
	private CustomerHandlerDAO customerHandlerDAO;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Customer> selectAllCustomers() throws Exception {
		return customerHandlerDAO.selectAllCustomers();
	}

	/**
	 * Delete customer
	 * 
	 * @param customer
	 * @return affected rows
	 * @throws Exception
	 */
	public int deleteCustomer(Customer customer) throws Exception {

		int affectedRows = customerHandlerDAO.deleteCustomer(customer);
		return affectedRows;
	}

	/**
	 * Update Customer details
	 * 
	 * @param customer
	 * @param itemToBeUpdated
	 * @return affected rows
	 * @throws Exception
	 */
	public int updateCustomer(Customer customer, String itemToBeUpdated) throws Exception {

		int affectedRows = 0;

		switch (itemToBeUpdated) {

		case CustomerHandlerConstants.FIRST_NAME:
			affectedRows = customerHandlerDAO.updateCustomerFirstName(customer);
			break;

		case CustomerHandlerConstants.LAST_NAME:
			affectedRows = customerHandlerDAO.updateCustomerLastName(customer);
			break;
			
		case CustomerHandlerConstants.STATE:
			affectedRows=customerHandlerDAO.updateCustomerState(customer);
			break;
			
		case CustomerHandlerConstants.CITY:
			affectedRows=customerHandlerDAO.updateCustomerCity(customer);
			break;
		}

		return affectedRows;
	}

	/**
	 * 
	 * @param customer
	 * @throws Exception
	 */
	public void addCustomer(Customer customer) throws Exception {
		customerHandlerDAO.addCustomer(customer);

	}

	/**
	 * 
	 * @return
	 */
	public CustomerHandlerDAO getCustomerHandlerDAO() {
		return customerHandlerDAO;
	}

	/**
	 * 
	 * @param customerHandlerDAO
	 */
	public void setCustomerHandlerDAO(CustomerHandlerDAO customerHandlerDAO) {
		this.customerHandlerDAO = customerHandlerDAO;
	}


	

}
