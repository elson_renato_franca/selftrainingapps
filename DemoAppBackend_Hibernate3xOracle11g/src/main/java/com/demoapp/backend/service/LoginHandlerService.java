package com.demoapp.backend.service;

import java.util.List;

import com.demoapp.backend.model.to.UserCredentialsTO;
import com.demoapp.backend.model.vo.UserCredentialsVO;

/**
 * 
 * 
 *
 */
public interface LoginHandlerService {

	/**
	 * 
	 * @param CredentialsVO
	 * @return
	 * @throws Exception
	 */
	public UserCredentialsTO findByLogin(UserCredentialsVO CredentialsVO) throws Exception;
	
	
	/**
	 * 
	 * @param CredentialsVO
	 * @return affectedRows
	 * @throws Exception
	 */
	public int saveUser(UserCredentialsVO CredentialsVO) throws Exception;
	
	/**
	 * 
	 * @param CredentialsVO
	 * @return affectedRows
	 * @throws Exception
	 */
	public int updateUser(UserCredentialsVO CredentialsVO) throws Exception;
	
	/**
	 * 
	 * @param CredentialsVO
	 * @return affectedRows
	 * @throws Exception
	 */
	public int deleteUser(UserCredentialsVO CredentialsVO) throws Exception;
	
	/**
	 * 
	 * @param CredentialsVO
	 * @return
	 * @throws Exception
	 */
	public List<UserCredentialsTO> findAll(UserCredentialsVO CredentialsVO) throws Exception;
	

}
