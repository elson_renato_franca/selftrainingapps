package com.demoapp.controller;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.backend.model.entities.Customer;
import com.demoapp.backend.service.CustomerHandlerService;


/**
 * 
 * 
 *
 */
public class DemoAppRunner {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("SpringBeans.xml");
		CustomerHandlerService customerHandlerService = (CustomerHandlerService)context.getBean("customerHandlerService");
		try {
			List<Customer> customerList = customerHandlerService.selectAllCustomers();
			
			if(customerList !=null){
				for(Customer customer: customerList){
					System.out.println(customer.getFirstName()+" "+customer.getLastName());
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
