package com.demoapp.backend.service.test;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.backend.model.to.UserCredentialsTO;
import com.demoapp.backend.model.vo.UserCredentialsVO;
import com.demoapp.backend.service.LoginHandlerService;

/**
 * 
 * 
 *
 */
public class UserHandlerTest {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(UserHandlerTest.class);
	

	/**
	 * 
	 */
	private ApplicationContext applicationContext;

	/**
	 * 
	 */
	private UserCredentialsVO userCredentialsVO;
	
	/**
	 * 
	 */
	private LoginHandlerService loginHandlerService;

	@Before
	public void setup(){
		this.applicationContext = new ClassPathXmlApplicationContext("SpringBeans.xml");
		this.loginHandlerService = (LoginHandlerService) applicationContext.getBean("loginHandlerService");
		this.userCredentialsVO =  (UserCredentialsVO) applicationContext.getBean("userCredentialsVO");
	
	}
	
	@Test
	public void findByLogin(){
		try {
			UserCredentialsTO userFound = this.loginHandlerService.findByLogin(userCredentialsVO);
			log.debug(userFound);
			assertNotNull(userFound);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void findAll(){
		try {
			List<UserCredentialsTO> userList = this.loginHandlerService.findAll(userCredentialsVO);
			log.debug(userList);
			assertNotNull(userList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@After
	public void tearDown(){
		this.applicationContext = null;
		this.loginHandlerService = null;
		this.userCredentialsVO =  null;
	}
	/**
	 * @return the applicationContext
	 */
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	/**
	 * @param applicationContext the applicationContext to set
	 */
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
	/**
	 * @return the userCredentialsVO
	 */
	public UserCredentialsVO getUserCredentialsVO() {
		return userCredentialsVO;
	}
	/**
	 * @param userCredentialsVO the userCredentialsVO to set
	 */
	public void setUserCredentialsVO(UserCredentialsVO userCredentialsVO) {
		this.userCredentialsVO = userCredentialsVO;
	}
	/**
	 * @return the loginHandlerService
	 */
	public LoginHandlerService getLoginHandlerService() {
		return loginHandlerService;
	}
	/**
	 * @param loginHandlerService the loginHandlerService to set
	 */
	public void setLoginHandlerService(LoginHandlerService loginHandlerService) {
		this.loginHandlerService = loginHandlerService;
	}
	
	
	

	
	
	
	
}
