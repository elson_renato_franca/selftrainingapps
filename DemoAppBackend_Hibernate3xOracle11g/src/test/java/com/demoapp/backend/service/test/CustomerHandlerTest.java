package com.demoapp.backend.service.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.springframework.context.ApplicationContext;

import com.demoapp.backend.model.vo.CustomerVO;
import com.demoapp.backend.service.CustomerHandlerService;


public class CustomerHandlerTest {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(CustomerHandlerTest.class);
	
	/**
	 * 
	 */
	private CustomerHandlerService customerHandlerService;
	
	

	/**
	 * 
	 */
	private ApplicationContext applicationContext;
	
	/**
	 * 
	 */
	private CustomerVO customerVO;
	
	@Before
	public void setup(){
		
	}
	@After
	public void tearDown(){
		
	}

	/**
	 * @return the customerHandlerService
	 */
	public CustomerHandlerService getCustomerHandlerService() {
		return customerHandlerService;
	}

	/**
	 * @param customerHandlerService the customerHandlerService to set
	 */
	public void setCustomerHandlerService(CustomerHandlerService customerHandlerService) {
		this.customerHandlerService = customerHandlerService;
	}

	/**
	 * @return the applicationContext
	 */
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * @param applicationContext the applicationContext to set
	 */
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	/**
	 * @return the customerVO
	 */
	public CustomerVO getCustomerVO() {
		return customerVO;
	}

	/**
	 * @param customerVO the customerVO to set
	 */
	public void setCustomerVO(CustomerVO customerVO) {
		this.customerVO = customerVO;
	}
	
	
	
	
}
