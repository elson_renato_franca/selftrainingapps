package com.demoapp.backend.service.test.suit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.demoapp.backend.service.test.CustomerHandlerTest;
import com.demoapp.backend.service.test.UserHandlerTest;


@RunWith(Suite.class)
@SuiteClasses({CustomerHandlerTest.class, UserHandlerTest.class })
public class AllTestCases {

}
