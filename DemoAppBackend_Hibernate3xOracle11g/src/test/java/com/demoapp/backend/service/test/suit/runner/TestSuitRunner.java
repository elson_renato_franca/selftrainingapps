package com.demoapp.backend.service.test.suit.runner;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import com.demoapp.backend.service.test.suit.AllTestCases;



public class TestSuitRunner {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(TestSuitRunner.class);
	
	/**
	 * 
	 * 
	 */
	public static void main(String[] args) {

		Result result = JUnitCore.runClasses(AllTestCases.class);

		for (Failure failure : result.getFailures()) {
			 log.error(failure.toString()); 
		}
		log.debug(result.wasSuccessful()); 

	}


}
