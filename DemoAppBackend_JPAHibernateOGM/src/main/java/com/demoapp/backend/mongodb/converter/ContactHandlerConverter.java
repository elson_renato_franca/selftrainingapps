package com.demoapp.backend.mongodb.converter;

import com.demoapp.backend.mongodb.entities.Contact;
import com.demoapp.backend.mongodb.model.to.ContactTO;
import com.demoapp.backend.mongodb.model.vo.ContactVO;

/**
 * 
 * 
 *
 */
public class ContactHandlerConverter {

	/**
	 * 
	 * @param contact
	 * @return ContactTO
	 * @throws Exception
	 */
	public ContactTO toContactTO(Contact contact) throws Exception {

		ContactTO contactTO = new ContactTO();

		contactTO.setAge(contact.getAge());
		contactTO.setEmail(contact.getEmail());
		contactTO.setFirstName(contact.getFirstName());
		contactTO.setId(contact.getId());
		contactTO.setSsid(contact.getSsid());
		contactTO.setLastName(contact.getLastName());
		contactTO.setPhoneDetails(contact.getPhoneDetails());
		contactTO.setAddress(contact.getAddress());

		return contactTO;
	}

	/**
	 * 
	 * @param contactVO
	 * @return
	 * @throws Exception
	 */
	public Contact fromVOtoContactEntity(ContactVO contactVO) throws Exception {
		Contact contact = new Contact();

		contact.setAge(contactVO.getAge());
		contact.setEmail(contactVO.getEmail());
		contact.setFirstName(contactVO.getFirstName());
		contact.setSsid(contactVO.getSsid());
		contact.setLastName(contactVO.getLastName());
		contact.setPhoneDetails(contactVO.getPhoneDetails());
		contact.setAddress(contactVO.getAddress());

		return contact;
	}

	/**
	 * 
	 * @param contactTO
	 * @return
	 * @throws Exception
	 */
	public Contact fromTOtoContactEntity(ContactTO contactTO) throws Exception {

		Contact contact = new Contact();

		contact.setAge(contactTO.getAge());
		contact.setEmail(contactTO.getEmail());
		contact.setFirstName(contactTO.getFirstName());
		contact.setId(contactTO.getId());
		contact.setSsid(contactTO.getSsid());
		contact.setLastName(contactTO.getLastName());
		contact.setPhoneDetails(contactTO.getPhoneDetails());
		contact.setAddress(contactTO.getAddress());

		return contact;
	}

}
