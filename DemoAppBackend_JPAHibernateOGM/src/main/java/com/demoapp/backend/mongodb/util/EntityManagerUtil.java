package com.demoapp.backend.mongodb.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.demoapp.backend.mongodb.dao.util.DemoAppQueries;

/**
 * 
 * It defines a single instance to be loaded only once at JVM
 *
 */
public class EntityManagerUtil {

	/**
	 * 
	 */
	private static EntityManager entityManager;

	/**
	 * 
	 */
	private EntityManagerUtil() {}

	/**
	 * Creates a new instance of entityManager if it doesn't exist
	 * @return
	 */
	public static synchronized EntityManager getInstance() {
		if (entityManager == null) {
			EntityManagerFactory entityManagerFactory = Persistence
					.createEntityManagerFactory(DemoAppQueries.PERSISTENCE_UNITY_NAME);
			entityManager = entityManagerFactory.createEntityManager();
		}
		return entityManager;
	}

	/**
	 * 
	 */
	public void closeEntityManagerInstance() {
		if (entityManager.isOpen() || entityManager != null) {
			entityManager.close();
		}

		entityManager = null;

	}

	/**
	 * 
	 * @return
	 */
	public static EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * 
	 * @param entityManager
	 */
	public static void setEntityManager(EntityManager entityManager) {
		EntityManagerUtil.entityManager = entityManager;
	}

}
