package com.demoapp.backend.mongodb.service;

import java.util.List;

import com.demoapp.backend.mongodb.model.to.ContactTO;
import com.demoapp.backend.mongodb.model.vo.ContactVO;

/**
 * 
 * 
 *
 */
public interface ContactHandlerService {

	/**
	 * Search for all contacts in MongoDB
	 * 
	 * @return List<ContactTO>
	 * @throws Exception
	 */
	public List<ContactTO> findAll() throws Exception;

	/**
	 * This method searches for contacts in MongoDB according to a specific name
	 * 
	 * @return Contact
	 * @throws Exception
	 */
	public ContactTO findContactByName(ContactVO contactVO) throws Exception;

	/**
	 * This method searches for contacts in MongoDB according to a specific id
	 * 
	 * @param ContactVO
	 * @return ContactTO
	 * @throws Exception
	 */
	public ContactTO findContactById(ContactVO contactVO) throws Exception;

	/**
	 * This method searches for contacts in MongoDB according to a specific
	 * social security number
	 * 
	 * @param ContactVO
	 *            contactVO
	 * @return ContactTO
	 * @throws Exception
	 */
	public ContactTO findContactBySSId(ContactVO contactVO) throws Exception;

	/**
	 * This method updates contact details
	 * 
	 * @param contactVO
	 *            contactVO
	 * @return affectedRows
	 * @throws Exception
	 */
	public int updateContact(ContactVO contactVO) throws Exception;

	/**
	 * This method removes the provided contactVO
	 * 
	 * @param ContactVO
	 *            contactVO
	 * @return affectedRows
	 * @throws Exception
	 */
	public int deleteContact(ContactVO contact) throws Exception;

	/**
	 * This method saves the provided contactVO
	 * 
	 * @param ContactVO
	 *            contactVO
	 * @return affectedRows
	 * @throws Exception
	 */
	public int saveContact(ContactVO contact) throws Exception;

}
