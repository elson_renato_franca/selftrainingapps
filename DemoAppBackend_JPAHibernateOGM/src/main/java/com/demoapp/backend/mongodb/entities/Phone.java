package com.demoapp.backend.mongodb.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

/**
 * 
 * 
 *
 */
@Embeddable
public class Phone implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private String number;
	
	/**
	 * 
	 */
	private String type;
	
	/**
	 * 
	 */
	@Embedded
	private PhoneServiceProvider phoneServiceProvider;

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the phoneServiceProvider
	 */
	public PhoneServiceProvider getPhoneServiceProvider() {
		return phoneServiceProvider;
	}

	/**
	 * @param phoneServiceProvider the phoneServiceProvider to set
	 */
	public void setPhoneServiceProvider(PhoneServiceProvider phoneServiceProvider) {
		this.phoneServiceProvider = phoneServiceProvider;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((phoneServiceProvider == null) ? 0 : phoneServiceProvider.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Phone other = (Phone) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (phoneServiceProvider == null) {
			if (other.phoneServiceProvider != null)
				return false;
		} else if (!phoneServiceProvider.equals(other.phoneServiceProvider))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PhoneDetails [number=" + number + ", type=" + type + ", phoneServiceProvider=" + phoneServiceProvider
				+ "]";
	}
	
	
	
	
}
