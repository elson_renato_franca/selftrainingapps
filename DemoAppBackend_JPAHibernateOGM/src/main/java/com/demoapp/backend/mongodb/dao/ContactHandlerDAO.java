package com.demoapp.backend.mongodb.dao;

import java.util.List;

import com.demoapp.backend.mongodb.entities.Contact;


/**
 * 
 * 
 *
 */
public interface ContactHandlerDAO {

	
	/**
	 * Search for all contacts in MongoDB
	 * @return List<Contact>
	 * @throws Exception
	 */
	public List<Contact> findAll()throws Exception;
		

	/**
	 * Update contact details
	 * @param contact
	 * @throws Exception
	 */
	public void updateContact(Contact contact)throws Exception;
	/**
	 * Remove contact details
	 * @param Contact contact
	 * @throws Exception
	 */
	public void deleteContact(Contact contact) throws Exception;
	/**
	 * Save contact
	 * @param contact
	 * @throws Exception
	 */
	public void saveContact(Contact contact) throws Exception;
	
	
	/**
	 * This method searches by Id
	 * @param contact
	 * @return
	 * @throws Exception
	 */
	public Contact findContactById(Contact contact)throws Exception;

	/**
	 * This method searches by social security number
	 * @param contact
	 * @return
	 * @throws Exception
	 */
	public Contact findContactBySSId(Contact contact)throws Exception;

	
	/**
	 * This method searches by name
	 * @param contact
	 * @return
	 * @throws Exception
	 */
	public Contact findContactByName(Contact contact)throws Exception;

}
