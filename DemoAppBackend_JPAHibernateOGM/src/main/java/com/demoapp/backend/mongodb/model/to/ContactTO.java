package com.demoapp.backend.mongodb.model.to;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.demoapp.backend.mongodb.entities.Address;
import com.demoapp.backend.mongodb.entities.Phone;

@XmlRootElement(name = "contact")
public class ContactTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private Integer id;
	
	/**
	 * 
	 */
	private String ssid;


	/**
	 * 
	 */
	private String firstName;

	/**
	 * 
	 */
	private String lastName;

	
	/**
	 * 
	 */
	private int age;

	/**
	 * 
	 */
	private String email;

	

	/**
	 * 
	 */
	@Embedded
	private Phone phoneDetails;
	

	/**
	 * 
	 */
	@Embedded
	private Address address;

	/**
	 * 
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	@XmlElement
	public void setId(Integer id) {
		this.id = id;
	}
	

	

	/**
	 * @return the ssid
	 */
	public String getSsid() {
		return ssid;
	}

	/**
	 * @param ssid the ssid to set
	 */
	@XmlElement
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	/**
	 * 
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * 
	 * @param firstName
	 */
	@XmlElement
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * 
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * 
	 * @param lastName
	 */
	@XmlElement
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * 
	 * @return
	 */
	public int getAge() {
		return age;
	}

	/**
	 * 
	 * @param age
	 */
	@XmlElement
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email
	 */
	@XmlElement
	public void setEmail(String email) {
		this.email = email;
	}

		/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @return the phoneDetails
	 */
	public Phone getPhoneDetails() {
		return phoneDetails;
	}

	/**
	 * @param phoneDetails the phoneDetails to set
	 */
	public void setPhoneDetails(Phone phoneDetails) {
		this.phoneDetails = phoneDetails;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + age;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phoneDetails == null) ? 0 : phoneDetails.hashCode());
		result = prime * result + ((ssid == null) ? 0 : ssid.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactTO other = (ContactTO) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (age != other.age)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phoneDetails == null) {
			if (other.phoneDetails != null)
				return false;
		} else if (!phoneDetails.equals(other.phoneDetails))
			return false;
		if (ssid == null) {
			if (other.ssid != null)
				return false;
		} else if (!ssid.equals(other.ssid))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ContactTO [id=" + id + ", ssid=" + ssid + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", age=" + age + ", email=" + email + ", phoneDetails=" + phoneDetails + ", address=" + address + "]";
	}

	
	

}
