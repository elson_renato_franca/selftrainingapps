package com.demoapp.backend.mongodb.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.demoapp.backend.mongodb.dao.ContactHandlerDAO;
import com.demoapp.backend.mongodb.entities.Contact;
import com.demoapp.backend.mongodb.util.EntityManagerUtil;

/**
 * 
 * Implements contact details service
 *
 */
public class ContactHandlerDAOImpl implements ContactHandlerDAO {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(ContactHandlerDAOImpl.class);

	/**
	 * Search for all contacts in MongoDB
	 * 
	 * @return List<Contact>
	 * @throws Exception
	 */
	public List<Contact> findAll() throws Exception {

		EntityManager entityManager = EntityManagerUtil.getInstance();
		Query query = entityManager.createQuery("from Contact");
		List<Contact> contacts = query.getResultList();
		return contacts;
	}

	/**
	 * Search for contacts in MongoDB according to a specific name
	 * 
	 * @return Contact
	 * @throws Exception
	 */
	public void updateContact(Contact contact) throws Exception {

		EntityManager entityManager = EntityManagerUtil.getInstance();
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		entityManager.merge(contact);
		transaction.commit();
	}

	@Override
	public void deleteContact(Contact contact) throws Exception {

		EntityManager entityManager = EntityManagerUtil.getInstance();
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		Object managedObject = entityManager.merge(contact);
		entityManager.remove(managedObject);
		transaction.commit();
	}

	@Override
	public void saveContact(Contact contact) throws Exception {

		EntityManager entityManager = EntityManagerUtil.getInstance();
		EntityTransaction transaction = entityManager.getTransaction();
		log.debug("Persisting Contact object");
		transaction.begin();
		entityManager.persist(contact);
		transaction.commit();

	}

	/**
	 * This method searches by Id
	 * 
	 * @param contact
	 * @return
	 * @throws Exception
	 */
	public Contact findContactById(Contact contact) throws Exception {

		EntityManager entityManager = EntityManagerUtil.getInstance();
		Contact resultQuery = (Contact) entityManager.getReference(Contact.class, contact.getId().intValue());
		return resultQuery;

	}

	/**
	 * This method searches by name
	 * 
	 * @param contact
	 * @return
	 * @throws Exception
	 */
	public Contact findContactByName(Contact contact) throws Exception {

		EntityManager entityManager = EntityManagerUtil.getInstance();
		Query query = entityManager.createQuery("from Contact c where c.firstName like :firstName ");
		query.setParameter("firstName", contact.getFirstName());
		Contact resultQuery = (Contact) query.getSingleResult();
		return resultQuery;
	}

	/**
	 * This method searches for contacts in MongoDB according to a specific
	 * social security number
	 * 
	 * @param ContactVO
	 *            contactVO
	 * @return ContactTO
	 * @throws Exception
	 */
	public Contact findContactBySSId(Contact contact) throws Exception {

		EntityManager entityManager = EntityManagerUtil.getInstance();
		Query query = entityManager.createQuery("from Contact c where c.ssid like :ssid ");
		query.setParameter("ssid", contact.getSsid());
		Contact resultQuery = (Contact) query.getSingleResult();
		
		return resultQuery;

	}

}
