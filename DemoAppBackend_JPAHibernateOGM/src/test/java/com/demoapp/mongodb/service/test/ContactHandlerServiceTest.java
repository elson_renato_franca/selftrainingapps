package com.demoapp.mongodb.service.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demoapp.backend.mongodb.model.to.ContactTO;
import com.demoapp.backend.mongodb.model.vo.ContactVO;
import com.demoapp.backend.mongodb.service.ContactHandlerService;


/**
 * 
 * 
 *
 */
public class ContactHandlerServiceTest {

	/**
	 * 
	 */
	private static final Log log = LogFactory.getLog(ContactHandlerServiceTest.class);
	
	/**
	 * 
	 */
	private ContactHandlerService contactService;
	
	/**
	 * 
	 */
	private ApplicationContext context;
	
	/**
	 * 
	 */
	private ContactVO contactVO;
	
	
	@Before
	public void setup(){
		
		this.context = new ClassPathXmlApplicationContext("SpringBeans.xml");
		this.contactService = (ContactHandlerService) context.getBean("contactHandlerService");
		this.contactVO =  (ContactVO) context.getBean("contactVO");
		
	}
	
	
	@Test
	public void saveContact(){
		
		try {
			int affectedRows = this.contactService.saveContact(this.contactVO);
			ContactTO resultQuery = this.contactService.findContactBySSId(contactVO);
			assertTrue(resultQuery !=null && affectedRows > 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void findAll(){
		try {
			List<ContactTO> resultList = this.contactService.findAll();
			assertNotNull(resultList);
			assertTrue(!resultList.isEmpty());
			
			if(resultList != null && !resultList.isEmpty()){
				for(ContactTO contactTO: resultList){
					log.debug(contactTO.toString());
					assertNotNull(contactTO);
					assertTrue(contactTO.getAge() > 0);
					assertNotNull(contactTO.getEmail());
					assertNotNull(contactTO.getFirstName());
					assertNotNull(contactTO.getLastName());
					assertNotNull(contactTO.getPhoneDetails().getNumber());
					assertNotNull(contactTO.getPhoneDetails().getType());
					assertNotNull(contactTO.getPhoneDetails().getPhoneServiceProvider().getName());
					assertNotNull(contactTO.getPhoneDetails().getPhoneServiceProvider().getNumber());
					assertNotNull(contactTO.getId());
					assertNotNull(contactTO.getSsid());
					assertNotNull(contactTO.getAddress().getCity());
					assertNotNull(contactTO.getAddress().getStreet());
					assertNotNull(contactTO.getAddress().getZipCode());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void findContactByName(){
		try {
			ContactTO resultQuery = this.contactService.findContactByName(this.contactVO);
			if(resultQuery != null){
				log.debug("Find by name: "+resultQuery.toString());	
			}
			assertNotNull(resultQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void findBySSID(){
		
		try {
			ContactTO resultQuery = this.contactService.findContactBySSId(contactVO);
			if(resultQuery != null){
				log.debug("Find by name: "+resultQuery.toString());	
			}
			assertNotNull(resultQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void updateContact(){
		try {
			int affectedRows = this.contactService.updateContact(this.contactVO);
			ContactTO resultQuery = this.contactService.findContactBySSId(contactVO);
			assertTrue(resultQuery !=null && affectedRows > 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void deleteContact(){
		
		try {
			int affectedRows = this.contactService.deleteContact(this.contactVO);
			ContactTO resultQuery = this.contactService.findContactBySSId(contactVO);
			assertTrue(resultQuery ==null && affectedRows > 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * @return
	 */
	public ContactHandlerService getContactService() {
		return contactService;
	}

	/**
	 * 
	 * @param contactService
	 */
	public void setContactService(ContactHandlerService contactService) {
		this.contactService = contactService;
	}

	/**
	 * 
	 * @return
	 */
	public ApplicationContext getContext() {
		return context;
	}

	/**
	 * 
	 * @param context
	 */
	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	/**
	 * 
	 * @return
	 */
	public ContactVO getContactVO() {
		return contactVO;
	}

	/**
	 * 
	 * @param contactVO
	 */
	public void setContactVO(ContactVO contactVO) {
		this.contactVO = contactVO;
	}


	
	
}
