package com.demoapp.training.javacore.threads.producerconsumer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 
 * BlockingQueue is a queue which is thread safe to insert or retrieve elements
 * from it. Also, it provides a mechanism which blocks requests for inserting
 * new elements when the queue is full or requests for removing elements when
 * the queue is empty, with the additional option to stop waiting when a
 * specific timeout passes. This functionality makes BlockingQueue a nice way of
 * implementing the Producer-Consumer pattern, as the producing thread can
 * insert elements until the upper limit of BlockingQueue while the consuming
 * thread can retrieve elements until the lower limit is reached and of course
 * with the support of the aforementioned blocking functionality.
 *
 */
public class BlockingQueueExample {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			BlockingQueue<Object> bq = new ArrayBlockingQueue<Object>(1000);
			Producer producer = new Producer(bq);
			Consumer consumer = new Consumer(bq);
			new Thread(producer).start();
			new Thread(consumer).start();
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
