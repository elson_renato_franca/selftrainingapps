package com.demoapp.training.javacore.arrays;

/**
 * 
 * This is a rather simple array interview question. You have given an unsorted
 * integer array and you need to find the largest and smallest element in the
 * array. Of course you can sort the array and then pick the top and bottom
 * element but that would cost you O(NLogN) because of sorting, getting element
 * in array with index is O(1) operation.
 * 
 * Read more:
 * http://javarevisited.blogspot.com/2015/06/top-20-array-interview-questions-and-answers.html#ixzz4HWKVV9Vi
 *
 * 
 */
public class MaximumAndMinimumValue {

	/**
	 * 
	 */
	private int smallest;

	/**
	 * 
	 */
	private int largest;

	/**
	 * 
	 * @return
	 */
	public void searchForSmallestLargest(int[] unsortedArray) {

		int largest = unsortedArray[0];
		int smallest = unsortedArray[0];

		if (unsortedArray != null) {
			for (int i = 0; i < unsortedArray.length; i++) {
				if (unsortedArray[i] > largest) {
					largest = unsortedArray[i];
				}
				if (unsortedArray[i] < smallest) {
					smallest = unsortedArray[0];
				}

			}
		}

		this.largest = largest;
		this.smallest = smallest;
	}

	/**
	 * @return the smallest
	 */
	public int getSmallest() {
		return smallest;
	}

	/**
	 * @param smallest
	 *            the smallest to set
	 */
	public void setSmallest(int smallest) {
		this.smallest = smallest;
	}

	/**
	 * @return the largest
	 */
	public int getLargest() {
		return largest;
	}

	/**
	 * @param largest
	 *            the largest to set
	 */
	public void setLargest(int largest) {
		this.largest = largest;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + largest;
		result = prime * result + smallest;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MaximumAndMinimumValue other = (MaximumAndMinimumValue) obj;
		if (largest != other.largest)
			return false;
		if (smallest != other.smallest)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MaximumAndMinimumValue [smallest=" + smallest + ", largest=" + largest + "]";
	}

}
