package com.demoapp.training.javacore.threads.forkjoin;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveAction;

/**
 * 
 * he fork/join framework is an implementation of the ExecutorService interface
 * that helps you take advantage of multiple processors. It is designed for work
 * that can be broken into smaller pieces recursively. The goal is to use all
 * the available processing power to enhance the performance of your
 * application.
 * 
 * As with any ExecutorService implementation, the fork/join framework
 * distributes tasks to worker threads in a thread pool. The fork/join framework
 * is distinct because it uses a work-stealing algorithm. Worker threads that
 * run out of things to do can steal tasks from other threads that are still
 * busy.
 *
 */
public class MyRecursiveAction extends RecursiveAction  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private long workLoad = 0;

	/**
	 * 
	 * @param workLoad
	 */
	public MyRecursiveAction(long workLoad) {
		this.workLoad = workLoad;
	}

	@Override
	protected void compute() {
		// if work is above threshold, break tasks up into smaller tasks
		if (this.workLoad > 16) {
			System.out.println("Splitting workLoad : " + this.workLoad);

			List<MyRecursiveAction> subtasks = new ArrayList<MyRecursiveAction>();

			subtasks.addAll(createSubtasks());

			for (RecursiveAction subtask : subtasks) {
				subtask.fork();
			}

		} else {
			System.out.println("Doing workLoad myself: " + this.workLoad);
		}

	}

	/**
	 * 
	 * @return
	 */
	private List<MyRecursiveAction> createSubtasks() {
		List<MyRecursiveAction> subtasks = new ArrayList<MyRecursiveAction>();

		MyRecursiveAction subtask1 = new MyRecursiveAction(this.workLoad / 2);
		MyRecursiveAction subtask2 = new MyRecursiveAction(this.workLoad / 2);

		subtasks.add(subtask1);
		subtasks.add(subtask2);

		return subtasks;
	}
	
	

}
