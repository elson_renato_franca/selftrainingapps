package com.demoapp.training.javacore.threads.producerconsumer;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

/**
 * 
 * 
 *
 */
public class Producer implements Runnable {

	/**
	 * 
	 */
	private BlockingQueue<Object> blockingQueue = null;
	
	/**
	 * 
	 * @param blockingQueue
	 */
	public Producer(BlockingQueue<Object> blockingQueue){
		this.setBlockingQueue(blockingQueue);
	}

	@Override
	public void run() {

		Random rand = new Random();
		int res = 0;
		try {
			res = Addition(rand.nextInt(100), rand.nextInt(50));
			System.out.println("Produced: " + res);
			blockingQueue.put(res);
			Thread.sleep(1000);
			res = Addition(rand.nextInt(100), rand.nextInt(50));
			System.out.println("Produced: " + res);
			blockingQueue.put(res);
			Thread.sleep(1000);
			res = Addition(rand.nextInt(100), rand.nextInt(50));
			System.out.println("Produced: " + res);
			blockingQueue.put(res);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public int Addition(int x, int y) {
		int result = 0;
		result = x + y;
		return result;
	}

	/**
	 * @return the blockingQueue
	 */
	public BlockingQueue<Object> getBlockingQueue() {
		return blockingQueue;
	}

	/**
	 * @param blockingQueue the blockingQueue to set
	 */
	public void setBlockingQueue(BlockingQueue<Object> blockingQueue) {
		this.blockingQueue = blockingQueue;
	}

	
	
}
