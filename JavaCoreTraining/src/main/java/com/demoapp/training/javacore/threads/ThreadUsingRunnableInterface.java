package com.demoapp.training.javacore.threads;

import com.demoapp.training.javacore.threads.test.MyCounter;

public class ThreadUsingRunnableInterface implements Runnable {

	@Override
	public void run() {
		System.out.println("Thread using Runnable interface - Starting");
		
		MyCounter myCounter = new MyCounter();
		
		for (int i = 0; i < 5; i++) {
			myCounter.increment();
		}

		System.out.println("Thread using Runnable interface - Started");
		
		
	}

}
