package com.demoapp.training.javacore.arrays;

import java.util.HashSet;
import java.util.Iterator;

/**
 * 
 * 
 *
 */
public class DuplicateArraysHashSet {

	/**
	 * set - o(N)
	 * does not guarantee order of elements returned - set property
	 * 
	 * @param input
	 * @return
	 */
	public static int[] removeDuplications(int[] input){
	    HashSet<Integer> myset = new HashSet<Integer>();

	    for( int i = 0; i < input.length; i++ ){
	        myset.add(input[i]);
	    }

	    //compact the array into the result.
	    int[] result = new int[myset.size()];
	    Iterator<Integer> setitr = myset.iterator();
	    int count = 0;
	    while( setitr.hasNext() ){
	        result[count] = (int) setitr.next();
	        count++;
	    }

	return result;
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		int[] a = { 1, 4, 5, 3, 5, 8, 6,1,3,9,0 };
		removeDuplications(a);
	}

}
