package com.demoapp.training.javacore.compareto;

public class Car implements Comparable<Car>{

	/**
	 * 
	 */
	private String licesingPlate;
	
	/**
	 * 
	 */
	private String model;
	
	/**
	 * 
	 */
	private String color;
	
	/**
	 * 
	 * @param licesingPlate
	 * @param model
	 * @param color
	 */
	public Car (String licesingPlate, String model, String color){
		this.licesingPlate = licesingPlate;
		this.model = model;
		this.color = color;
	}

	/**
	 * @return the licesingPlate
	 */
	public String getLicesingPlate() {
		return licesingPlate;
	}

	/**
	 * @param licesingPlate the licesingPlate to set
	 */
	public void setLicesingPlate(String licesingPlate) {
		this.licesingPlate = licesingPlate;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public int compareTo(Car o) {
		return this.model.compareTo(o.model);
	}
	
}
