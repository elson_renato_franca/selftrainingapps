package com.demoapp.training.javacore.arrays;

import java.util.Arrays;

public class ArrayContains {


	/**
	 * Search for a number in a sorted array
	 * @param array
	 * @param key
	 * @return
	 */
	public int binarySearch(int [] array, int key){
		 Arrays.sort(array);
		 int searchValue = Arrays.binarySearch(array,key);
		 return searchValue;
	}
	
	/**
	 * Search for a key in an unsorted array
	 * @param unsortedArray
	 * @param key
	 * @return
	 */
	public int linearSearch( int [] unsortedArray, int key){
		
		int valueFound =-1;
		
		if(unsortedArray !=null){
			for(int i=0; i< unsortedArray.length;i++){
				if(unsortedArray[i] == key){
					valueFound=unsortedArray[i];
				}
			}	
		}
		
		return valueFound;
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String [] args){
		
		 int [] originalArray= { 1, 4, 5, 3, 5, 8, 6,1,3,9,0 };
		 int wantedNumber = 9;
		 ArrayContains arrayContains = new ArrayContains(); 
		 int linearSearchResult=arrayContains.linearSearch(originalArray, wantedNumber);
		 if(linearSearchResult == -1){
			 System.out.println("Number not found");
		 }else{
			 System.out.println("Number found: "+linearSearchResult);
		 }
		 
		 int binarySearchResult = arrayContains.binarySearch(originalArray, wantedNumber);
		 System.out.println(binarySearchResult);
	}
}
