package com.demoapp.training.javacore.threads.lock.concurrent.randomnumbers;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * Synchronized code relies on a simple kind of reentrant lock. This kind of
 * lock is easy to use, but has many limitations. More sophisticated locking
 * idioms are supported by the java.util.concurrent.locks package. We won't
 * examine this package in detail, but instead will focus on its most basic
 * interface, Lock.
 * 
 * Lock objects work very much like the implicit locks used by synchronized
 * code. As with implicit locks, only one thread can own a Lock object at a
 * time. Lock objects also support a wait/notify mechanism, through their
 * associated Condition objects.
 * 
 * The biggest advantage of Lock objects over implicit locks is their ability to
 * back out of an attempt to acquire a lock. The tryLock method backs out if the
 * lock is not available immediately or before a timeout expires (if specified).
 * The lockInterruptibly method backs out if another thread sends an interrupt
 * before the lock is acquired.
 *
 */
public class SafeLockConcurrentRandomNumber {

	/**
	 * 
	 * 
	 *
	 */
	static class Friend {

		/**
		 * 
		 */
		private final String name;

		/**
		 * 
		 */
		private final Lock lock = new ReentrantLock();

		/**
		 * 
		 * @param name
		 */
		public Friend(String name) {
			this.name = name;
		}

		/**
		 * 
		 * @return
		 */
		public String getName() {
			return this.name;
		}

		/**
		 * 
		 * Acquires the lock if it is not held by another thread and returns
		 * immediately with the value true, setting the lock hold count to one.
		 * Even when this lock has been set to use a fair ordering policy, a
		 * call to tryLock() will immediately acquire the lock if it is
		 * available, whether or not other threads are currently waiting for the
		 * lock. This "barging" behavior can be useful in certain circumstances,
		 * even though it breaks fairness. If you want to honor the fairness
		 * setting for this lock, then use tryLock(0, TimeUnit.SECONDS) which is
		 * almost equivalent (it also detects interruption).
		 * 
		 * If the current thread already holds this lock then the hold count is
		 * incremented by one and the method returns true.
		 * 
		 * If the lock is held by another thread then this method will return
		 * immediately with the value false.
		 * 
		 * source:
		 * http://docs.oracle.com/javase/7/docs/api/java/util/concurrent/locks/ReentrantLock.html#tryLock()
		 * 
		 * @param bower
		 * @return
		 */
		public boolean impendingBow(Friend bower) {
			Boolean myLock = false;
			Boolean yourLock = false;
			try {
				// Acquires the lock only if it is not held by another thread at
				// the time of invocation.
				myLock = lock.tryLock();
				yourLock = bower.lock.tryLock();
			} finally {
				if (!(myLock && yourLock)) {
					if (myLock) {
						/**
						 * Attempts to release this lock.
						 * 
						 * If the current thread is the holder of this lock then
						 * the hold count is decremented. If the hold count is
						 * now zero then the lock is released. If the current
						 * thread is not the holder of this lock then
						 * IllegalMonitorStateException is thrown.
						 */
						lock.unlock();
					}
					if (yourLock) {
						bower.lock.unlock();
					}
				}
			}
			return myLock && yourLock;
		}

		/**
		 * 
		 * @param bower
		 */
		public void bow(Friend bower) {
			if (impendingBow(bower)) {
				try {
					System.out.format("%s: %s has" + " bowed to me!%n", this.name, bower.getName());
					bower.bowBack(this);
				} finally {
					lock.unlock();
					bower.lock.unlock();
				}
			} else {
				System.out.format(
						"%s: %s started" + " to bow to me, but saw that" + " I was already bowing to" + " him.%n",
						this.name, bower.getName());
			}
		}

		public void bowBack(Friend bower) {
			System.out.format("%s: %s has" + " bowed back to me!%n", this.name, bower.getName());
		}
	}

	/**
	 * 
	 * Threads
	 *
	 */
	static class BowLoop implements Runnable {
		
		/**
		 * 
		 */
		private Friend bower;
		
		/**
		 * 
		 */
		private Friend bowee;

		/**
		 * 
		 * @param bower
		 * @param bowee
		 */
		public BowLoop(Friend bower, Friend bowee) {
			this.bower = bower;
			this.bowee = bowee;
		}

		/**
		 * 
		 */
		public void run() {
			for (;;) {
				try {
					
					int concurrentRandomNumber = ThreadLocalRandom.current().nextInt(1,10);
					Thread.sleep(concurrentRandomNumber);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				bowee.bow(bower);
			}
		}
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		final Friend alphonse = new Friend("Alphonse");
		final Friend gaston = new Friend("Gaston");
		new Thread(new BowLoop(alphonse, gaston)).start();
		new Thread(new BowLoop(gaston, alphonse)).start();
	}
}
