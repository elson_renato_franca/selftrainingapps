package com.demoapp.training.javacore.threads;

import com.demoapp.training.javacore.threads.test.MyCounter;

/**
 * 
 * 
 *
 */
public class ThreadExtendingThreadObject extends Thread{


	/**
	 * 
	 */
	public void run (){
		System.out.println("Thread extending Thread Object - starting");
		
		MyCounter myCounter = new MyCounter();
		
		for (int i = 0; i < 5; i++) {
			myCounter.decrement();
		}
		System.out.println("Thread extending Thread Object - started");
		

	}
}
