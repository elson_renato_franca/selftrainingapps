package com.demoapp.training.javacore.threads.atomicvariable;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 * 
 *
 */
public class MyAtomicCounter {

	/**
	 * 
	 */
	private AtomicInteger c = new AtomicInteger(0);

	/**
	 * 
	 */
	public void increment() {
		c.incrementAndGet();
	}

	/**
	 * 
	 */
	public void decrement() {
		c.decrementAndGet();
	}

	/**
	 * 
	 * @return
	 */
	public int value() {
		return c.get();
	}
}
