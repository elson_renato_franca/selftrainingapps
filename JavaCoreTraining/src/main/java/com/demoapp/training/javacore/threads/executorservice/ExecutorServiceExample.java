package com.demoapp.training.javacore.threads.executorservice;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Most of the executor implementations in java.util.concurrent use thread
 * pools, which consist of worker threads. This kind of thread exists separately
 * from the Runnable and Callable tasks it executes and is often used to execute
 * multiple tasks.
 * 
 * Using worker threads minimizes the overhead due to thread creation. Thread
 * objects use a significant amount of memory, and in a large-scale application,
 * allocating and deallocating many thread objects creates a significant memory
 * management overhead.
 * 
 * One common type of thread pool is the fixed thread pool. This type of pool
 * always has a specified number of threads running; if a thread is somehow
 * terminated while it is still in use, it is automatically replaced with a new
 * thread. Tasks are submitted to the pool via an internal queue, which holds
 * extra tasks whenever there are more active tasks than threads.
 * 
 * An important advantage of the fixed thread pool is that applications using it
 * degrade gracefully. To understand this, consider a web server application
 * where each HTTP request is handled by a separate thread. If the application
 * simply creates a new thread for every new HTTP request, and the system
 * receives more requests than it can handle immediately, the application will
 * suddenly stop responding to all requests when the overhead of all those
 * threads exceed the capacity of the system. With a limit on the number of the
 * threads that can be created, the application will not be servicing HTTP
 * requests as quickly as they come in, but it will be servicing them as quickly
 * as the system can sustain.
 * 
 * source: https://docs.oracle.com/javase/tutorial/essential/concurrency/pools.html
 */
public class ExecutorServiceExample {

	/**
	 * 
	 */
	private ExecutorService executorService;

	/**
	 * 
	 */
	public ExecutorServiceExample() {
		this.executorService = Executors.newFixedThreadPool(10);
	}

	/**
	 * 
	 */
	public void executeExecutorServiceByRunnable() {

		executorService.execute(new Runnable() {
			public void run() {
				System.out.println("Asynchronous task by Runnable");
			}
		});

		shutdownExecuteService();

	}

	/**
	 * 
	 */
	public void shutdownExecuteService() {
		executorService.shutdown();
	}

	/**
	 * @throws ExecutionException
	 * @throws InterruptedException
	 * 
	 */
	public void executeExecutorServiceBySubmit() throws InterruptedException, ExecutionException {

		Future<?> future = executorService.submit(new Runnable() {
			public void run() {
				System.out.println("Asynchronous task - By Submit");
			}
		});

		future.get(); // returns null if the task has finished correctly.

	}

	/**
	 * 
	 * @param callable
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	public void executeExecutorServiceByCallable() throws InterruptedException, ExecutionException {

		Future<Object> future = executorService.submit(new Callable<Object>() {
			public Object call() throws Exception {
				System.out.println("Asynchronous Callable");
				return "Callable Result";
			}
		});

		System.out.println("future.get() = " + future.get());
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		ExecutorServiceExample executorServiceExample = new ExecutorServiceExample();
		try {
			executorServiceExample.executeExecutorServiceByCallable();
			executorServiceExample.executeExecutorServiceBySubmit();
			executorServiceExample.executeExecutorServiceByRunnable();

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the executorService
	 */
	public ExecutorService getExecutorService() {
		return executorService;
	}

	/**
	 * @param executorService
	 *            the executorService to set
	 */
	public void setExecutorService(ExecutorService executorService) {
		this.executorService = executorService;
	}

}
