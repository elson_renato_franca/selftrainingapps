package com.demoapp.training.javacore.arrays.sort;

/**
 * 
 * Quicksort is a divide and conquer algorithm, which means original list is
 * divided into multiple list, each of them is sorted individually and then
 * sorted output is merged to produce the sorted list.
 * 
 * 1) Choose an element, called pivot, from the list or array. Generally pivot
 * is the middle element of array.
 * 
 * 2) Reorder the list so that all elements with values less than the pivot come
 * before the pivot, and all elements with values greater than the pivot come
 * after it (equal values can go either way). This is also known as
 * partitioning. After partitioning the pivot is in its final position.
 * 
 * 3) Recursively apply the above steps to the sub-list of elements with smaller
 * values and separately the sub-list of elements with greater values. If the
 * array contains only one element or zero elements then the array is sorted.
 * 
 * Read more:
 * http://javarevisited.blogspot.com/2014/08/quicksort-sorting-algorithm-in-java-in-place-example.html#ixzz4HWdr3LrS
 * 
 * @author Javin Paul
 */
public class QuickSort {

	/**
	 * 
	 */
	private int input[];
    
	/**
	 * 
	 */
	private int length;

    /**
     * 
     * @param numbers
     */
    public void sort(int[] numbers) {

        if (numbers == null || numbers.length == 0) {
            return;
        }
        this.input = numbers;
        length = numbers.length;
        quickSort(0, length - 1);
    }

    
    /**
     * This method implements in-place quicksort algorithm recursively.
     * @param low
     * @param high
     */
    private void quickSort(int low, int high) {
        int i = low;
        int j = high;

        // pivot is middle index
        int pivot = input[low + (high - low) / 2];

        // Divide into two arrays
        while (i <= j) {
            /**
             * As shown in above image, In each iteration, we will identify a
             * number from left side which is greater then the pivot value, and
             * a number from right side which is less then the pivot value. Once
             * search is complete, we can swap both numbers.
             */
            while (input[i] < pivot) {
                i++;
            }
            while (input[j] > pivot) {
                j--;
            }
            if (i <= j) {
                swap(i, j);
                // move index to next position on both sides
                i++;
                j--;
            }
        }

        // calls quickSort() method recursively
        if (low < j) {
            quickSort(low, j);
        }

        if (i < high) {
            quickSort(i, high);
        }
    }

    /**
     * 
     * @param i
     * @param j
     */
    private void swap(int i, int j) {
        int temp = input[i];
        input[i] = input[j];
        input[j] = temp;
    }


}
