package com.demoapp.training.javacore.threads.producerconsumer;

import java.util.concurrent.BlockingQueue;

/**
 * 
 * 
 *
 */
public class Consumer implements Runnable {

	/**
	 * 
	 */
	protected BlockingQueue<Object> queue = null;

	/**
	 * 
	 * @param queue
	 */
	public Consumer(BlockingQueue<Object> queue) {
		this.queue = queue;
	}

	/**
	 * 
	 */
	public void run() {
		try {
			System.out.println("Consumed: " + queue.take());
			System.out.println("Consumed: " + queue.take());
			System.out.println("Consumed: " + queue.take());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
