package com.demoapp.training.javacore.threads.atomicvariable;

/**
 * 
 * 
 *
 */
public class CounterSynchronized {

	/**
	 * 
	 */
	private int c = 0;

	/**
	 * 
	 */
	public synchronized void increment() {
		c++;
	}

	/**
	 * 
	 */
	public synchronized void decrement() {
		c--;
	}

	/**
	 * 
	 * @return
	 */
	public synchronized int value() {
		return c;
	}
}
