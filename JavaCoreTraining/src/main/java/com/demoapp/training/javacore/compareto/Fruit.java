package com.demoapp.training.javacore.compareto;

/**
 * 
 * 
 *
 */
public class Fruit  implements Comparable<Fruit>{

	/**
	 * 
	 */
	private String fruitName;
	
	/**
	 * 
	 */
	private String fruitDesc;
	
	/**
	 * 
	 */
	private int quantity;
	
	/**
	 * 
	 * @param fruitName
	 * @param fruitDesc
	 * @param quantity
	 */
	public Fruit(String fruitName, String fruitDesc, int quantity) {
		super();
		this.fruitName = fruitName;
		this.fruitDesc = fruitDesc;
		this.quantity = quantity;
	}

	/**
	 * @return the fruitName
	 */
	public String getFruitName() {
		return fruitName;
	}

	/**
	 * @param fruitName the fruitName to set
	 */
	public void setFruitName(String fruitName) {
		this.fruitName = fruitName;
	}

	/**
	 * @return the fruitDesc
	 */
	public String getFruitDesc() {
		return fruitDesc;
	}

	/**
	 * @param fruitDesc the fruitDesc to set
	 */
	public void setFruitDesc(String fruitDesc) {
		this.fruitDesc = fruitDesc;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public int compareTo(Fruit o) {
		
		return this.fruitName.compareTo(o.fruitName);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Fruit [fruitName=" + fruitName + ", fruitDesc=" + fruitDesc + ", quantity=" + quantity + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fruitDesc == null) ? 0 : fruitDesc.hashCode());
		result = prime * result + ((fruitName == null) ? 0 : fruitName.hashCode());
		result = prime * result + quantity;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fruit other = (Fruit) obj;
		if (fruitDesc == null) {
			if (other.fruitDesc != null)
				return false;
		} else if (!fruitDesc.equals(other.fruitDesc))
			return false;
		if (fruitName == null) {
			if (other.fruitName != null)
				return false;
		} else if (!fruitName.equals(other.fruitName))
			return false;
		if (quantity != other.quantity)
			return false;
		return true;
	}
	
	
	
}
