package com.demoapp.training.javacore.arrays;

import java.util.Arrays;

/**
 *  Brute force- O(N square)
 * 
 *  The running time of the loop is directly proportional to N. When N
 *  doubles, so does the running time.The running time of the two loops is
 *  proportional to the square of N. When N doubles, the running time
 *  increases by N * N.
 *
 */

public class DuplicateArraysBruteForce {

	/**
	 * Brute force- O(N square)
	 * 
	 * @param initialArray
	 * @return int
	 */
	public static int[] searchForDuplicates(int[] initialArray) {

		int size = initialArray.length;
		int[] noDuplicatedArrays = new int[size];
		int sameNums = 0;
		boolean[] isSame = new boolean[size];

		if (initialArray != null) {
			for (int i = 0; i < initialArray.length; i++) {
				for (int j = i + 1; j < initialArray.length; j++) {
					if (initialArray[i] == initialArray[j]) {
						isSame[j] = true;
						sameNums++;
					}

				}

			}

			// compact the array into the result.
			int[] result = new int[size - sameNums];
			int count = 0;
			for (int i = 0; i < initialArray.length; i++) {
				if (isSame[i] == true) {
					continue;
				} else {
					result[count] = initialArray[i];
					count++;
				}
			}

			noDuplicatedArrays = result;
		}

		return noDuplicatedArrays;

	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		int[] a = { 1, 4, 5, 3, 5, 8, 6,1,3,9,0 };
		int [] result=searchForDuplicates(a);
		System.out.println(Arrays.toString(result));
		
	}

}
