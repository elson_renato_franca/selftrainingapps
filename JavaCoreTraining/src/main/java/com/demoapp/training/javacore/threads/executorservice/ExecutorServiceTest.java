package com.demoapp.training.javacore.threads.executorservice;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * 
 *
 */
public class ExecutorServiceTest {

	/**
	 * 
	 */
	private ExecutorServiceExample executorServiceExample;
	
	/**
	 * 
	 */
	private ExecutorService executorService;
	
	@Before
	public void setup(){
		this.executorServiceExample = new ExecutorServiceExample(); 
		this.executorService = Executors.newFixedThreadPool(10);
	}
	
	@Test
	public void testExecutorByRunnable(){
	
		this.executorService.execute(new Runnable() {
		    public void run() {
		        System.out.println("Asynchronous task");
		    }
		});
		
	}

	/**
	 * @return the executorServiceExample
	 */
	public ExecutorServiceExample getExecutorServiceExample() {
		return executorServiceExample;
	}

	/**
	 * @param executorServiceExample the executorServiceExample to set
	 */
	public void setExecutorServiceExample(ExecutorServiceExample executorServiceExample) {
		this.executorServiceExample = executorServiceExample;
	}

	/**
	 * @return the executorService
	 */
	public ExecutorService getExecutorService() {
		return executorService;
	}

	/**
	 * @param executorService the executorService to set
	 */
	public void setExecutorService(ExecutorService executorService) {
		this.executorService = executorService;
	}
	
	
	
}
