package com.demoapp.training.javacore.threads.test;

import org.junit.Before;
import org.junit.Test;

import com.demoapp.training.javacore.threads.ThreadExtendingThreadObject;


public class ThreadExtendingThreadTest {

	/**
	 * 
	 */
	private ThreadExtendingThreadObject threadExtendingThread;

	
	@Before
	public void setup(){
		this.threadExtendingThread = new ThreadExtendingThreadObject(); 
	}
	
	@Test	
	public void startThreadExtendingThread(){
		  this.threadExtendingThread.start();
	}
	
	/**
	 * @return the threadExtendingThread
	 */
	public ThreadExtendingThreadObject getThreadExtendingThread() {
		return threadExtendingThread;
	}

	/**
	 * @param threadExtendingThread the threadExtendingThread to set
	 */
	public void setThreadExtendingThread(ThreadExtendingThreadObject threadExtendingThread) {
		this.threadExtendingThread = threadExtendingThread;
	}
	
	
	
	
	
	
}
