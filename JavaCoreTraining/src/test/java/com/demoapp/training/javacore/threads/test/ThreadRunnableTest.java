package com.demoapp.training.javacore.threads.test;

import org.junit.Before;
import org.junit.Test;

import com.demoapp.training.javacore.threads.ThreadUsingRunnableInterface;

/**
 * 
 * 
 *
 */
public class ThreadRunnableTest {

	/**
	 * 
	 */
	private ThreadUsingRunnableInterface threadRunnable;
	
	@Before
	public void setup(){
		this.threadRunnable = new ThreadUsingRunnableInterface();
	}
	
	@Test
	public void startThreadRunnable(){
		new Thread(threadRunnable).start();
	}

	
	public void startThreadExtendingThread(){
		
	}
	/**
	 * @return the threadRunnable
	 */
	public ThreadUsingRunnableInterface getThreadRunnable() {
		return threadRunnable;
	}

	/**
	 * @param threadRunnable the threadRunnable to set
	 */
	public void setThreadRunnable(ThreadUsingRunnableInterface threadRunnable) {
		this.threadRunnable = threadRunnable;
	}


}
