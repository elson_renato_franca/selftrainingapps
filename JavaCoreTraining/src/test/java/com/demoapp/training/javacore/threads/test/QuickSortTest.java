package com.demoapp.training.javacore.threads.test;

import static org.junit.Assert.assertNotNull;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.demoapp.training.javacore.arrays.MaximumAndMinimumValue;
import com.demoapp.training.javacore.arrays.sort.QuickSort;

/**
 * 
 * Test class to sort array of integers using Quicksort algorithm in Java.
 * @author Javin Paul
 */
public class QuickSortTest {

	/**
	 * 
	 */
	private int [] originalArray;
	
	/**
	 * 
	 */
	private QuickSort quickSort;
	
	@Before
	public void setup(){
		
		quickSort = new QuickSort();
		
		originalArray = new int[100];
		
		for (int i = 0; i < 100; i++) {
			originalArray[i] = (int) (Math.random() * 10);
		}	 
	}
	
	@Test
	public void sortArray(){
		this.quickSort.sort(originalArray);
		
		 // printing sorted array
        System.out.println("Sorted array :" + Arrays.toString(originalArray));
        
		assertNotNull(originalArray);
		
	}

	/**
	 * @return the originalArray
	 */
	public int[] getOriginalArray() {
		return originalArray;
	}

	/**
	 * @param originalArray the originalArray to set
	 */
	public void setOriginalArray(int[] originalArray) {
		this.originalArray = originalArray;
	}

	/**
	 * @return the quickSort
	 */
	public QuickSort getQuickSort() {
		return quickSort;
	}

	/**
	 * @param quickSort the quickSort to set
	 */
	public void setQuickSort(QuickSort quickSort) {
		this.quickSort = quickSort;
	}
	
	
	
}
