package com.demoapp.training.javacore.threads.forkjoin.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.demoapp.training.javacore.threads.forkjoin.MyRecursiveAction;

/**
 * 
 * 
 *
 */
public class MyRecursiveActionTest {
	/**
	 * 
	 */
	private MyRecursiveAction myRecursiveAction;
	
	/**
	 * 
	 */
	private int workLoad;
	
	@Before
	public void setup(){
		this.workLoad = 24;
		myRecursiveAction = new MyRecursiveAction(this.getWorkLoad()); 
	}
	
	@Test
	public void processSplittedTasks(){
		this.myRecursiveAction.invokeAll(myRecursiveAction);
	}
	
	@After
	public void tearDown(){
		this.workLoad = 0;
		this.myRecursiveAction = null;
	}

	/**
	 * @return the myRecursiveAction
	 */
	public MyRecursiveAction getMyRecursiveAction() {
		return myRecursiveAction;
	}

	/**
	 * @param myRecursiveAction the myRecursiveAction to set
	 */
	public void setMyRecursiveAction(MyRecursiveAction myRecursiveAction) {
		this.myRecursiveAction = myRecursiveAction;
	}

	/**
	 * @return the workLoad
	 */
	public int getWorkLoad() {
		return workLoad;
	}

	/**
	 * @param workLoad the workLoad to set
	 */
	public void setWorkLoad(int workLoad) {
		this.workLoad = workLoad;
	}
	
	
	
	
}
