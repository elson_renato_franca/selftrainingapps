package com.demoapp.training.javacore.threads.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.demoapp.training.javacore.arrays.MaximumAndMinimumValue;

/**
 * 
 * 
 *
 */
public class SmallestAndLargestNumberTest {

	/**
	 * 
	 */
	private int [] originalArray;
	
	/**
	 * 
	 */
	private MaximumAndMinimumValue object;
	
	@Before
	public void setup(){
		
		object = new MaximumAndMinimumValue();
		
		originalArray = new int[100];
		
		for (int i = 0; i < 100; i++) {
			originalArray[i] = (int) (Math.random() * 10);
		}	 
	}
	
	@Test
	public void checkTheSmallestAndTheLargest(){
		int [] unsortedArray = this.originalArray;
		this.object.searchForSmallestLargest(unsortedArray);
		System.out.println("Largest number is: "+this.object.getLargest());
		System.out.println("Smallest number is: "+this.object.getSmallest());
		assertNotNull(this.object.getLargest());
		assertNotNull(this.object.getSmallest());
	}

	/**
	 * @return the originalArray
	 */
	public int[] getOriginalArray() {
		return originalArray;
	}

	/**
	 * @param originalArray the originalArray to set
	 */
	public void setOriginalArray(int[] originalArray) {
		this.originalArray = originalArray;
	}

	/**
	 * @return the object
	 */
	public MaximumAndMinimumValue getObject() {
		return object;
	}

	/**
	 * @param object the object to set
	 */
	public void setObject(MaximumAndMinimumValue object) {
		this.object = object;
	}

	
	
}
