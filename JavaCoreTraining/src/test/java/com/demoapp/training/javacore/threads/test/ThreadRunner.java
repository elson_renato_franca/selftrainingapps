package com.demoapp.training.javacore.threads.test;

import com.demoapp.training.javacore.threads.ThreadExtendingThreadObject;
import com.demoapp.training.javacore.threads.ThreadUsingRunnableInterface;

public class ThreadRunner {

	/**
	 * 
	 * 
	 */
	public static void main(String [] args){
		 ThreadUsingRunnableInterface p = new ThreadUsingRunnableInterface();
		 ThreadExtendingThreadObject t = new ThreadExtendingThreadObject(); 
		 
		 new Thread(p).start();
		 t.start();
		 
	     

	}
}
