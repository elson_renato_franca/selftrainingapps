package com.demoapp.training.javacore.threads.test;

public class MyCounter {

	/**
	 * 
	 */
	private int value=0;
	
	/**
	 * 
	 */
	public void increment(){
		value++;
		System.out.println("Value now: "+value);
	}
	
	/**
	 * 
	 */
	public void decrement(){
		value --;
		System.out.println("Value now: "+value);
	}
	
	/**
	 * 
	 */
	public void multiply(){
		value*=value;
		System.out.println("Value multiplied by "+value+"="+value);
	}
	/**
	 * 
	 */
	public void divide(){
		value=value%2;
		System.out.println("Value divide by "+value +"="+value);
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}
	
	
}
