package com.demoapp.training.javacore.threads.test;

import static org.junit.Assert.assertNotNull;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.demoapp.training.javacore.compareto.Fruit;

/**
 * 
 * 
 *
 */
public class FruitComparatorTest {

	/**
	 * 
	 */
	private Fruit[] fruitArray;

	@Before
	public void setup() {

		this.fruitArray = new Fruit[4];

		Fruit pineappale = new Fruit("Pineapple", "Pineapple description", 70);
		Fruit apple = new Fruit("Apple", "Apple description", 100);
		Fruit orange = new Fruit("Orange", "Orange description", 80);
		Fruit banana = new Fruit("Banana", "Banana description", 90);

		fruitArray[0] = pineappale;
		fruitArray[1] = apple;
		fruitArray[2] = orange;
		fruitArray[3] = banana;

	}

	@Test
	public void sortFruits() {
		Arrays.sort(fruitArray);

		if (fruitArray != null) {
			for (Fruit fruit : fruitArray) {
				System.out.println(fruit.getFruitName().toString());
			}
		}

		assertNotNull(fruitArray);
	}

	/**
	 * @return the fruitArray
	 */
	public Fruit[] getFruitArray() {
		return fruitArray;
	}

	/**
	 * @param fruitArray
	 *            the fruitArray to set
	 */
	public void setFruitArray(Fruit[] fruitArray) {
		this.fruitArray = fruitArray;
	}

}
