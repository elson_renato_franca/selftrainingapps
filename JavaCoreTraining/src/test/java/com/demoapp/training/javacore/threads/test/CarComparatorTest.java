package com.demoapp.training.javacore.threads.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.demoapp.training.javacore.compareto.Car;

public class CarComparatorTest {

	/**
	 * 
	 */
	private List<Car> carList;

	@Before
	public void setup(){
		carList = new ArrayList<Car>();
		Car car1 = new Car("ABC 647468","Chevrolet","red");
		Car car2 = new Car("ABC 247468","Ford","Blue");
		Car car3 = new Car("ABC 677468","BMW","Purple");
		Car car4 = new Car("ABC 45668","Porsch","Black");
		Car car5 = new Car("ABC 123456","Aston Martin","White");
		carList.add(car1);
		carList.add(car2);
		carList.add(car3);
		carList.add(car4);
		carList.add(car5);
	}
	
	@Test
	public void sortCarList(){
		Collections.sort(carList);
		
		if (carList != null) {
			for (Car car : carList) {
				System.out.println(car.getModel().toString()+","+car.getLicesingPlate().toString()+","+car.getColor().toString());
			}
		}
		
		assertNotNull(carList);

	}
}
