package com.demoapp.training.hamcrest.examples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.demoapp.training.javacore.hamcrest.beans.HamcrestExampleBean;

public class ExampleHamcrestTest {

	/**
	 * 
	 */
	private List<Integer> list;
	
	/**
	 * 
	 */
	private HamcrestExampleBean bean;

	@Before
	public void setup() {
		list = new ArrayList<Integer>();
		list.add(5);
		list.add(3);
		list.add(2);
		list.add(7);
		// list.add(null);
		list.add(8);
		
		this.bean = new HamcrestExampleBean(1, "Learn Hamcrest", "Important"); 
	}
	
	@Test
	public void objectHasSummaryProperty () {
	    Assert.assertThat(bean, Matchers.hasProperty("summary"));
	}
	
	@Test
	public void objectHasCorrectSummaryValue () {

		 Assert.assertThat(bean, Matchers.hasProperty("summary", Matchers.equalTo("Learn Hamcrest")));
	}

	@Test
	public void sortList() {

		Collections.sort(list);

		Assert.assertEquals(4, list.size());
		Assert.assertEquals(new Integer(2), list.get(0));
		Assert.assertEquals(new Integer(3), list.get(1));
		Assert.assertEquals(new Integer(5), list.get(2));
		Assert.assertEquals(new Integer(7), list.get(3));

	}

	@Test
	public void sortWithHamcrest() {

		Collections.sort(list);
		Assert.assertThat(list, Matchers.contains(2, 3, 5, 7));
	}

	@Test
	public void checkListSize() {
		Assert.assertThat(list, Matchers.hasSize(4));
	}

	@Test
	public void checkEmptyList() {
		Assert.assertThat(list, Matchers.is(Matchers.empty()));
	}

	@Test
	public void containsInAnyOrder() {
		Assert.assertThat(list, Matchers.containsInAnyOrder(3, 5, 7, 2));
	}

	@Test
	public void checkForNotEmptyList() {
		Assert.assertThat(list, Matchers.is(Matchers.notNullValue()));
	}

	@Test
	public void checkForNotEmptyItemListUsingFor() {
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				Integer value = list.get(i);
				Assert.assertThat(value, Matchers.is(Matchers.notNullValue()));
			}
		}

	}

	@Test
	public void checkForNotEmptyItemListWithoutFor() {

		Assert.assertThat(list, Matchers.everyItem(Matchers.not(null)));
	}

	@Test
	public void iterateAListWithoutUsingFor() {
		List<String> listString = new ArrayList<String>();
		listString.add("1x");
		listString.add("2x");
		listString.add("3x");
		listString.add("4x");

		Assert.assertThat(listString, Matchers.everyItem(Matchers.endsWith("x")));

		Assert.assertThat(listString, Matchers.contains(Matchers.notNullValue()));

	}

	@Test
	public void collectionContainsElements() {
		Assert.assertThat(list, Matchers.hasItems(2, 5));
	}

	@Test
	public void elementIsOneOf() {
		Assert.assertThat(2, Matchers.isOneOf(list.toArray()));
	}

	/**
	 * 
	 */
	@After
	public void tearDown() {
		this.list = null;
	}

	/**
	 * @return the list
	 */
	public List<Integer> getList() {
		return list;
	}

	/**
	 * @param list
	 *            the list to set
	 */
	public void setList(List<Integer> list) {
		this.list = list;
	}

	/**
	 * @return the bean
	 */
	public HamcrestExampleBean getBean() {
		return bean;
	}

	/**
	 * @param bean the bean to set
	 */
	public void setBean(HamcrestExampleBean bean) {
		this.bean = bean;
	}

	
}
