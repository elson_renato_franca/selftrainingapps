package com.demoapp.training.hamcrest.examples;

import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * 
 * 
 *
 */
public class FirstElementMatcher extends TypeSafeMatcher<List<Integer>>{

	@Override
	public void describeTo(Description description) {
		description.appendText("List starting with element 2");
	}

	@Override
	protected boolean matchesSafely(List<Integer> list) {

		if(list != null && list.size() > 0){
			return list.get(0) == 2;
		}
		
		return false;
	}



}
