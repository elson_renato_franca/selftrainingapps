package com.demoapp.training.hamcrest.examples;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * 
 *
 */
public class CustomizedMatcherTest {

	/**
	 * 
	 */
	private List<Integer> list;

	@Before
	public void setup() {
		list = new ArrayList<>();
		list.add(5);
		list.add(3);
		list.add(2);
		list.add(7);
	}
	
	@Test
	public void checkCustomizedTest(){
		Collections.sort(list);
		Assert.assertThat(list, new FirstElementMatcher());
	}
	
	@After
	public void tearDown(){
		list = null;
	}

}
